package com.globalia.application.service;

import com.globalia.HelperTest;
import com.globalia.application.service.Group;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.MasterItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashSet;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupTest extends HelperTest {

	@InjectMocks
	private Group translate;


	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testTranslateGroupDelete() {
		GroupCredentialItem group = group();
		group.setDeleted(true);
		this.translate.translateGroup(group);
		assertTrue(true);
	}

	@Test
	public void testTranslateGroupNullRedis() {
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		this.translate.translateGroup(group());
		assertTrue(true);
	}

	@Test
	public void testTranslateGroupIOException() throws IOException {
		GroupCredentialItem group = group();
		group.setTagCurrency(true);
		group.setTagNationality(true);

		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		this.translate.translateGroup(group);
		assertTrue(true);
	}

	@Test
	public void testTranslateGroupCreate() throws IOException {
		GroupCredentialItem group = group();
		group.setCreate(true);
		group.setAllMasters(new HashSet<>());
		group.getAllMasters().add("rate_type");
		when(this.redisLoader.getRediskey(anyString(), anyBoolean())).thenReturn("key");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(new MasterItem());
		this.translate.translateGroup(group);
		assertTrue(true);
	}
}
