package com.globalia.application.service;

import com.globalia.HelperTest;
import com.globalia.dto.DateRange;
import com.globalia.dto.credential.ExternalCredentialItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ConnectorTest extends HelperTest {

	@InjectMocks
	private Connector translate;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testTranslateExternalDelete() {
		ExternalCredentialItem connector = connector();
		connector.setDeleted(true);
		this.translate.translateExternal(connector);
		assertTrue(true);
	}

	@Test
	public void testTranslateExternalUpdate() {
		when(this.client.get(anyString(), anyString())).thenReturn("1");
		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(new Date());
		this.translate.translateExternal(connector());
		assertTrue(true);
	}

	@Test
	public void testTranslateExternalCreate() {
		ExternalCredentialItem connector = connector();
		connector.setCreate(true);
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(new Date());
		this.translate.translateExternal(connector);
		assertTrue(true);
	}

	@Test
	public void testTranslateExternalCreateDatesDft() {
		ExternalCredentialItem connector = connector();
		connector.setCreate(true);
		connector.setCheckIn(new DateRange());
		connector.setBook(new DateRange());
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(new Date());
		this.translate.translateExternal(connector);
		assertTrue(true);
	}
}
