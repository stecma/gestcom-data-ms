package com.globalia.application.service;

import com.globalia.HelperTest;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.InfoStatic;
import com.globalia.enumeration.credential.MasterType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SupplierTest extends HelperTest {

	@InjectMocks
	private Supplier translate;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testTranslateSupplierDelete() {
		SupplierItem supplier = supplier();
		supplier.setDeleted(true);
		supplier.setCodes(Set.of(pairValue(MasterType.EXTERNAL_SYSTEM, "1")));
		this.translate.translateSupplier(supplier);
		assertTrue(true);
	}

	@Test
	public void testTranslateSupplierNoSystem() {
		SupplierItem supplier = supplier();
		supplier.setCodes(Set.of(pairValue(MasterType.EXTERNAL_SYSTEM, "")));
		this.translate.translateSupplier(supplier);
		assertTrue(true);
	}

	@Test
	public void testTranslateSupplierNet() {
		SupplierItem supplier = supplier();
		supplier.getCodes().add(pairValue(MasterType.BUY_MODEL, "N"));

		when(this.redisLoader.getRediskey(nullable(String.class), anyBoolean())).thenReturn("key");
		this.translate.translateSupplier(supplier);
		assertTrue(true);
	}

	@Test
	public void testTranslateSupplierCom() {
		SupplierItem supplier = supplier();
		supplier.getCodes().add(pairValue(MasterType.BUY_MODEL, "C"));
		supplier.setStaticInfo(InfoStatic.ONLINE);

		when(this.redisLoader.getRediskey(nullable(String.class), anyBoolean())).thenReturn("key");
		this.translate.translateSupplier(supplier);
		assertTrue(true);
	}

	@Test
	public void testTranslateSupplierNoSubSystem() {
		SupplierItem supplier = supplier();
		supplier.setCodes(Set.of(pairValue(MasterType.EXTERNAL_SYSTEM, "1")));

		when(this.redisLoader.getRediskey(nullable(String.class), anyBoolean())).thenReturn("key");
		this.translate.translateSupplier(supplier);
		assertTrue(true);
	}
}
