package com.globalia.application.mapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ItemMapperTest {

	@InjectMocks
	private ItemMapper itemMapper;
	@Mock
	private ResultSet resultSet;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		this.itemMapper = new ItemMapper();
	}

	@Test
	public void testMapRow() throws SQLException {
		when(this.resultSet.getString("id")).thenReturn("COMPANY");
		when(this.resultSet.getString("json")).thenReturn("TEST");
		assertNotNull(this.itemMapper.mapRow(this.resultSet, 1));
	}
}
