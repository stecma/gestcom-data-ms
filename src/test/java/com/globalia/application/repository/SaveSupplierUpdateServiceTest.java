package com.globalia.application.repository;

import com.globalia.HelperTest;
import com.globalia.application.repository.SaveSupplier;
import com.globalia.dto.credential.SupplierItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveSupplierUpdateServiceTest extends HelperTest {

	@InjectMocks
	private SaveSupplier save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testUpdateServiceNoQuery() {
		SupplierItem supplier = supplier();
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		this.save.updateService(supplier.getSystem().getId(), supplier.getSystem().getServiceOther(), supplier.getSystem().getDescSrvOther());
		assertTrue(true);
	}

	@Test
	public void testUpdateService() {
		SupplierItem supplier = supplier();
		supplier.getSystem().setId("1");
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenReturn(1);
		this.save.updateService(supplier.getSystem().getId(), supplier.getSystem().getServiceOther(), supplier.getSystem().getDescSrvOther());
		assertTrue(true);
	}

	@Test
	public void testUpdateServiceSub() {
		SupplierItem supplier = supplier();
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenReturn(1);
		this.save.updateService(supplier.getSystem().getId(), supplier.getSystem().getServiceOther(), supplier.getSystem().getDescSrvOther());
		assertTrue(true);
	}
}
