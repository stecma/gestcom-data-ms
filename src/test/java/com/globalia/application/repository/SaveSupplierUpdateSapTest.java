package com.globalia.application.repository;

import com.globalia.HelperTest;
import com.globalia.application.repository.SaveSupplier;
import com.globalia.dto.credential.SupplierItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveSupplierUpdateSapTest extends HelperTest {

	@InjectMocks
	private SaveSupplier save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testUpdateSapNoQuery() {
		SupplierItem supplier = supplier();
		supplier.getSystem().setId("1");
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		this.save.updateSap(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), true);
		assertTrue(true);
	}

	@Test
	public void testUpdateSap() {
		SupplierItem supplier = supplier();
		supplier.getSystem().setId("1");
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenReturn(1);
		this.save.updateSap(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), true);
		assertTrue(true);
	}

	@Test
	public void testUpdateSapProvNoQuery() {
		SupplierItem supplier = supplier();
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		this.save.updateSap(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), false);
		assertTrue(true);
	}

	@Test
	public void testUpdateSapProv() {
		SupplierItem supplier = supplier();
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenReturn(1);
		this.save.updateSap(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), false);
		assertTrue(true);
	}

	@Test
	public void testUpdateSapInsertProvNoQuery() {
		SupplierItem supplier = supplier();
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		this.save.updateSap(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), true);
		assertTrue(true);
	}

	@Test
	public void testUpdateSapInsertProv() {
		SupplierItem supplier = supplier();
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenReturn(1);
		this.save.updateSap(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), true);
		assertTrue(true);
	}
}
