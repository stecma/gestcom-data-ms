package com.globalia.application.repository;

import com.globalia.HelperTest;
import com.globalia.application.repository.GetItems;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.dto.ItemDao;
import com.globalia.application.mapper.ItemMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GetItemsTest extends HelperTest {

	@InjectMocks
	private GetItems getItems;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testGetItemsNoQuery() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		assertNull(this.getItems.getItems(SqlLoader.sqlFile.MASTER));
	}

	@Test
	public void testGetItems() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.query(anyString(), any(ItemMapper.class))).thenReturn(Collections.singletonList(new ItemDao()));
		assertNotNull(this.getItems.getItems(SqlLoader.sqlFile.MASTER));
	}

	@Test
	public void testGetItemsScan() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.query(anyString(), any(ItemMapper.class))).thenReturn(Collections.singletonList(new ItemDao()));
		assertNotNull(this.getItems.getItems(SqlLoader.sqlFile.MASTER.name(), true));
	}

	@Test
	public void testGetSupplierByZoneNoQuery() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		assertNull(this.getItems.getSupplierByZone(supplierByZone()));
	}

	@Test
	public void testGetSupplierByZoneEmptyResultDataAccessException() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.queryForObject(anyString(), any(ItemMapper.class), any())).thenThrow(EmptyResultDataAccessException.class);
		assertNull(this.getItems.getSupplierByZone(supplierByZone()));
	}

	@Test
	public void testGetSupplierByZone() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.queryForObject(anyString(), any(ItemMapper.class), any())).thenReturn(new ItemDao());
		assertNotNull(this.getItems.getSupplierByZone(supplierByZone()));
	}

	@Test
	public void testGetMasterNoQuery() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		assertNull(this.getItems.getMaster(master()));
	}

	@Test
	public void testGetMasterEmptyResultDataAccessException() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.queryForObject(anyString(), any(ItemMapper.class), any())).thenThrow(EmptyResultDataAccessException.class);
		assertNull(this.getItems.getMaster(master()));
	}

	@Test
	public void testGetMaster() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.queryForObject(anyString(), any(ItemMapper.class), any())).thenReturn(new ItemDao());
		assertNotNull(this.getItems.getMaster(master()));
	}

	@Test
	public void testGetAllChildItemsEmptyMap() {
		when(this.sqlLoader.getChildSqlMap(anyString())).thenReturn(new HashMap<>());
		assertTrue(this.getItems.getAllChildItems(SqlLoader.sqlFile.MASTER.name()).isEmpty());
	}

	@Test
	public void testGetAllChildItemsNoQuery() {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("0¬INSERT", "QUERY");

		when(this.sqlLoader.getChildSqlMap(anyString())).thenReturn(queryMap);
		assertTrue(this.getItems.getAllChildItems(SqlLoader.sqlFile.MASTER.name()).isEmpty());
	}

	@Test
	public void testGetAllChildItemQueryNull() {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("0¬INSERT", "QUERY");
		queryMap.put("0¬QUERY", "QUERY");

		when(this.sqlLoader.getChildSqlMap(anyString())).thenReturn(queryMap);
		when(this.jdbcTemplate.query(anyString(), any(ItemMapper.class))).thenReturn(null);
		assertTrue(this.getItems.getAllChildItems(SqlLoader.sqlFile.MASTER.name()).isEmpty());
	}

	@Test
	public void testGetAllChildItemQueryEmpty() {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("0¬INSERT", "QUERY");
		queryMap.put("0¬QUERY", "QUERY");

		when(this.sqlLoader.getChildSqlMap(anyString())).thenReturn(queryMap);
		when(this.jdbcTemplate.query(anyString(), any(ItemMapper.class))).thenReturn(new ArrayList<>());
		assertTrue(this.getItems.getAllChildItems(SqlLoader.sqlFile.MASTER.name()).isEmpty());
	}

	@Test
	public void testGetAllChildItem() {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("0¬INSERT", "QUERY");
		queryMap.put("0¬QUERY", "QUERY");

		when(this.sqlLoader.getChildSqlMap(anyString())).thenReturn(queryMap);
		when(this.jdbcTemplate.query(anyString(), any(ItemMapper.class))).thenReturn(List.of(new ItemDao()));
		assertFalse(this.getItems.getAllChildItems(SqlLoader.sqlFile.MASTER.name()).isEmpty());
	}
}
