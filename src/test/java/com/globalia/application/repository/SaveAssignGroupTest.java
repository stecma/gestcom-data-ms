package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.infraestructure.SqlLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveAssignGroupTest extends HelperTest {

	@InjectMocks
	private SaveAssignGroup save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testGetParams() throws JsonProcessingException {
		assertNotNull(this.save.getParams(SqlLoader.sqlType.DELETE_ASSIGN, group()));
	}

	@Test
	public void testAssignIOException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		assertNotNull(this.save.assign("credential", "json").getError());
	}

	@Test
	public void testAssignError() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.sqlLoader.getSql(any(), any())).thenReturn(null);
		assertNotNull(this.save.assign("credential", "json").getError());
	}

	@Test
	public void testAssign() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenReturn(1);
		this.save.assign("credential", "json");
		assertTrue(true);
	}

	@Test
	public void testCreateItemJsonProcessingException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNotNull(this.save.createItem("credential", group()));
	}

	@Test
	public void testCreateItemNoQuery() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.sqlLoader.getSql(any(), any())).thenReturn(null);
		assertNotNull(this.save.createItem("credential", group()).getError());
	}

	@Test
	public void testCreateItemRuntimeException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenThrow(RuntimeException.class);
		assertNotNull(this.save.createItem("credential", group()).getError());
	}

	@Test
	public void testCreateItem() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenReturn(1);
		assertNull(this.save.createItem("credential", group()).getError());
	}

	@Test
	public void testDeleteItemJsonProcessingException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNotNull(this.save.deleteItem("credential", group()));
	}

	@Test
	public void testDeleteItemNoQuery() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.sqlLoader.getSql(any(), any())).thenReturn(null);
		assertNotNull(this.save.deleteItem("credential", group()).getError());
	}

	@Test
	public void testDeleteItemRuntimeException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenThrow(RuntimeException.class);
		assertNotNull(this.save.deleteItem("credential", group()).getError());
	}

	@Test
	public void testDeleteItem() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group());
		when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenReturn(1);
		assertNull(this.save.deleteItem("credential", group()).getError());
	}
}
