package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.application.repository.SaveSupplier;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.MasterType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveSupplierTest extends HelperTest {

	@InjectMocks
	private SaveSupplier save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testSendToBBDDCreateIOException() throws IOException {
		assertNotNull(this.sendToBBDD("create", true, false, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDCreateItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, true, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDCreateItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, true, false, null).getError());
	}

	@Test
	public void testSendToBBDDCreateItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, false, true, null).getError());
	}

	@Test
	public void testSendToBBDDCreateItemRemoveChildrenNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, false, false, "removeChild").getError());
	}

	@Test
	public void testSendToBBDDCreateItemRemoveChildrenRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, false, false, "removeChildIO").getError());
	}

	@Test
	public void testSendToBBDDCreateItemAddChildrenNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, false, false, "addChild").getError());
	}

	@Test
	public void testSendToBBDDCreateItemAddChildrenRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, false, false, "addChildIO").getError());
	}

	@Test
	public void testSendToBBDDCreateItemNoCodes() throws IOException {
		SupplierItem supplier = supplier();
		supplier.setCodes(null);
		assertNotNull(this.sendToBBDD(supplier, "create", false, false, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDCreateItemEmptyCodes() throws IOException {
		SupplierItem supplier = supplier();
		supplier.setCodes(new HashSet<>());
		assertNotNull(this.sendToBBDD(supplier, "create", false, false, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDCreateItemNoAgencyNoDestinations() throws IOException {
		SupplierItem supplier = supplier();
		supplier.setCodes(Set.of(pairValue(MasterType.BRAND, new LinkedHashSet<>(Set.of("1")))));
		assertNull(this.sendToBBDD(supplier, "create", false, false, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDCreateItem() throws IOException {
		assertNull(this.sendToBBDD("create", false, false, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDUpdateIOException() throws IOException {
		assertNotNull(this.sendToBBDD("update", true, false, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, true, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, false, true, false, null).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, false, false, true, null).getError());
	}

	@Test
	public void testSendToBBDDUpdateItem() throws IOException {
		assertNull(this.sendToBBDD("update", false, false, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDDeleteIOException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", true, false, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, true, false, false, null).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, false, true, false, null).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, false, false, true, null).getError());
	}

	@Test
	public void testSendToBBDDDeleteItem() throws IOException {
		assertNull(this.sendToBBDD("delete", false, false, false, false, null).getError());
	}

	private ItemResponse sendToBBDD(final String action, final boolean isIOException, final boolean isJsonException, final boolean isNoQuery, final boolean isRunException, final String extra) throws IOException {
		return this.sendToBBDD(supplier(), action, isIOException, isJsonException, isNoQuery, isRunException, extra);
	}

	private ItemResponse sendToBBDD(final SupplierItem supplier, final String action, final boolean isIOException, final boolean isJsonException, final boolean isNoQuery, final boolean isRunException, final String extra) throws IOException {
		if (isIOException) {
			when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		} else {
			when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(supplier);
		}
		if (isJsonException) {
			when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		} else {
			when(this.jsonHandler.toJson(any())).thenReturn("json");
		}
		if (isNoQuery) {
			when(this.sqlLoader.getSql(any(), any())).thenReturn(null);
		} else {
			when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		}
		if (isRunException) {
			when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenThrow(RuntimeException.class);
		} else {
			when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenReturn(1);
		}

		if (StringUtils.hasText(extra)) {
			switch (extra) {
				case "removeChild":
					when(this.sqlLoader.getSql(SqlLoader.sqlFile.SUPPLIER.name().toLowerCase(), String.format("%s¬%s", SqlLoader.sqlType.DELETE.name().toLowerCase(), MasterType.TOP_DESTINATION.name().toLowerCase()))).thenReturn(null);
					break;
				case "removeChildIO":
					when(this.sqlLoader.getSql(SqlLoader.sqlFile.SUPPLIER.name().toLowerCase(), String.format("%s¬%s", SqlLoader.sqlType.DELETE.name().toLowerCase(), MasterType.AGENCY.name().toLowerCase()))).thenReturn("SELECT * FROM dual");
					when(this.jdbcTemplate.update("SELECT * FROM dual", "1")).thenThrow(RuntimeException.class);
					break;
				case "addChild":
					when(this.sqlLoader.getSql(SqlLoader.sqlFile.SUPPLIER.name().toLowerCase(), String.format("%s¬%s", SqlLoader.sqlType.INSERT.name().toLowerCase(), MasterType.TOP_DESTINATION.name().toLowerCase()))).thenReturn(null);
					break;
				default:
					when(this.sqlLoader.getSql(SqlLoader.sqlFile.SUPPLIER.name().toLowerCase(), String.format("%s¬%s", SqlLoader.sqlType.INSERT.name().toLowerCase(), MasterType.TOP_DESTINATION.name().toLowerCase()))).thenReturn("SELECT * FROM dual");
					when(this.jdbcTemplate.update("SELECT * FROM dual", new Object[]{supplier.getId(), "1"})).thenThrow(RuntimeException.class);
			}
		}
		ItemResponse response = this.save.sendToBBDD(action, "json");
		this.save.translate(SqlLoader.sqlFile.SUPPLIER, supplier);
		return response;
	}
}
