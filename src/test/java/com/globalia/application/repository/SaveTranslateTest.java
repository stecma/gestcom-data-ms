package com.globalia.application.repository;

import com.globalia.HelperTest;
import com.globalia.application.repository.SaveTranslate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveTranslateTest extends HelperTest {

	@InjectMocks
	private SaveTranslate save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testTranslateGroupId() {
		when(this.jdbcTemplate.queryForObject(anyString(), (Class<Object>) any())).thenReturn(1L);
		assertNotNull(this.save.getGroupId());
	}

	@Test
	public void testTranslateSupplierNoQuery() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		this.save.translateSupplier("update¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testTranslateSupplierDataIntegrityViolationException() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenThrow(DataIntegrityViolationException.class);
		this.save.translateSupplier("update¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testTranslateSupplierRuntimeException() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenThrow(RuntimeException.class);
		this.save.translateSupplier("update¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testTranslateSupplier() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenReturn(1);
		this.save.translateSupplier("update¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testTranslateGroupNoQuery() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		this.save.translateGroup("update¬group¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testtTranslateGroupDataIntegrityViolationException() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenThrow(DataIntegrityViolationException.class);
		this.save.translateGroup("update¬group¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testtTranslateGroupRuntimeException() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenThrow(RuntimeException.class);
		this.save.translateGroup("update¬group¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testTranslateGroup() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenReturn(1);
		this.save.translateGroup("update¬group¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testTranslateConnectorNoQuery() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn(null);
		this.save.translateExternal("update¬connector¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testtTranslateConnectorDataIntegrityViolationException() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenThrow(DataIntegrityViolationException.class);
		this.save.translateExternal("update¬connector¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testtTranslateConnectorRuntimeException() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenThrow(RuntimeException.class);
		this.save.translateExternal("update¬connector¬supplier", new Object[0], "dev");
		assertTrue(true);
	}

	@Test
	public void testTranslateConnector() {
		when(this.sqlLoader.getSql(anyString(), anyString())).thenReturn("query");
		when(this.jdbcTemplate.update(anyString(), (Object) any())).thenReturn(1);
		this.save.translateExternal("update¬connector¬supplier", new Object[0], "dev");
		assertTrue(true);
	}
}
