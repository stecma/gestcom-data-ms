package com.globalia.application.consumer;

import com.globalia.HelperTest;
import com.globalia.dto.KafkaItem;
import com.globalia.application.repository.SaveSupplier;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MessageConsumerReplyUpdateServiceTest extends HelperTest {

	@InjectMocks
	private MessageConsumerReply consumer;
	@Mock
	private SaveSupplier save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testProcessMessageIOException() throws InterruptedException, IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		this.consumer.processMessage("addserviceitem");
		assertTrue(true);
	}

	@Test
	public void testProcessMessageEmpty() throws IOException, InterruptedException {
		KafkaItem message = message("addserviceitem");
		message.setJson(null);
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message);
		this.consumer.processMessage("addserviceitem");
		assertTrue(true);
	}

	@Test
	public void testProcessMessageIncomplete() throws IOException, InterruptedException {
		KafkaItem message = message("addserviceitem");
		message.setJson("1#2");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message);
		this.consumer.processMessage("addserviceitem");
		assertTrue(true);
	}

	@Test
	public void testProcessMessage() throws IOException, InterruptedException {
		KafkaItem message = message("addserviceitem");
		message.setJson("1#2#3");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message);
		this.consumer.processMessage("addserviceitem");
		assertTrue(true);
	}
}
