package com.globalia.application.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.application.repository.SaveRelease;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MessageConsumerReplyReleaseTest extends HelperTest {

	@InjectMocks
	private MessageConsumerReply consumer;
	@Mock
	private SaveRelease save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testProcessMessageIOException() throws IOException, InterruptedException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		assertNull(this.consumer.processMessage("release"));
	}

	@Test
	public void testProcessMessageJsonProcessingException() throws IOException, InterruptedException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("release"));
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		when(this.save.sendToBBDD(anyString(), anyString())).thenReturn(new ItemResponse());
		assertNull(this.consumer.processMessage("release"));
	}

	@Test
	public void testProcessMessage() throws IOException, InterruptedException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("release"));
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.save.sendToBBDD(anyString(), anyString())).thenReturn(new ItemResponse());
		assertNotNull(this.consumer.processMessage("release"));
	}
}
