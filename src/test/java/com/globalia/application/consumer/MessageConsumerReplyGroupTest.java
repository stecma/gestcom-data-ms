package com.globalia.application.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.exception.Error;
import com.globalia.application.repository.SaveGroup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MessageConsumerReplyGroupTest extends HelperTest {

	@InjectMocks
	private MessageConsumerReply consumer;
	@Mock
	private SaveGroup save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testProcessMessageIOException() throws IOException, InterruptedException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		assertNull(this.consumer.processMessage("group"));
	}

	@Test
	public void testProcessMessageJsonProcessingException() throws IOException, InterruptedException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("group"));
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		when(this.save.sendToBBDD(anyString(), anyString())).thenReturn(new ItemResponse());
		assertNull(this.consumer.processMessage("group"));
	}

	@Test
	public void testProcessMessageError() throws IOException, InterruptedException {
		ItemResponse response = new ItemResponse();
		response.setError(new Error());
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("group"));
		when(this.save.sendToBBDD(anyString(), anyString())).thenReturn(response);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNotNull(this.consumer.processMessage("group"));
	}

	@Test
	public void testProcessMessage() throws IOException, InterruptedException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("group"));
		when(this.save.sendToBBDD(anyString(), anyString())).thenReturn(new ItemResponse());
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNotNull(this.consumer.processMessage("group"));
	}
}
