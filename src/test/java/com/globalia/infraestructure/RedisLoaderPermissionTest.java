package com.globalia.infraestructure;

import com.globalia.HelperTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class RedisLoaderPermissionTest extends HelperTest {

	private static final String PERMISSION = "{ \"entities\": [ { \"entity\": \"BRAND\", \"permission\": \"{\\\"newAllowed\\\": false,\\\"updAllowed\\\": false,\\\"delAllowed\\\": false}\" }, { \"entity\": \"DEPTH\", \"permission\": \"{\\\"newAllowed\\\": false,\\\"updAllowed\\\": false,\\\"delAllowed\\\": false}\" } ] }";
	private static final String PERMISSION_EMPTY = "{ \"entities\": []}";

	@InjectMocks
	private RedisLoader redisLoader;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testLoaderPermissionNull() {
		this.redisLoader.loaderPermission();
		assertTrue(true);
	}

	@Test
	public void testLoaderPermissionEmpty() {
		init(RedisLoader.class, this.redisLoader, "masterPermission", RedisLoaderPermissionTest.PERMISSION_EMPTY);
		this.redisLoader.loaderPermission();
		assertTrue(true);
	}

	@Test
	public void testLoaderPermissionRuntimeException() {
		init(RedisLoader.class, this.redisLoader, "masterPermission", RedisLoaderPermissionTest.PERMISSION);
		doThrow(RuntimeException.class).when(this.client).addMap(anyString(), anyMap());
		this.redisLoader.loaderPermission();
		assertTrue(true);
	}

	@Test
	public void testLoaderPermission() {
		init(RedisLoader.class, this.redisLoader, "masterPermission", RedisLoaderPermissionTest.PERMISSION);
		this.redisLoader.loaderPermission();
		assertTrue(true);
	}
}
