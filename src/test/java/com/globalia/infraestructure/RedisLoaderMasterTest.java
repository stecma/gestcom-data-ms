package com.globalia.infraestructure;

import com.globalia.HelperTest;
import com.globalia.dto.ItemDao;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.infraestructure.SqlLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class RedisLoaderMasterTest extends HelperTest {

	@InjectMocks
	private RedisLoader redisLoader;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		init(RedisLoader.class, this.redisLoader, "mastersKey", "key");
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testLoaderMasterNoItems() {
		when(this.getItems.getItems(any(), anyBoolean())).thenReturn(new ArrayList<>());
		this.redisLoader.loaderMaster();
		assertTrue(true);
	}

	@Test
	public void testLoaderRuntimeException() {
		ItemDao item1 = new ItemDao();
		item1.setId("1");
		item1.setValue("json");

		when(this.getItems.getItems(any(), anyBoolean())).thenReturn(new ArrayList<>());
		when(this.getItems.getItems(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, MasterType.BRAND.name()).toLowerCase(), false)).thenReturn(Collections.singletonList(item1));
		doThrow(RuntimeException.class).when(this.client).addMap(anyString(), anyMap());
		this.redisLoader.loaderMaster();
		assertTrue(true);
	}

	@Test
	public void testLoaderScanRuntimeException() {
		ItemDao item1 = new ItemDao();
		item1.setId("1");
		item1.setValue("json");

		when(this.getItems.getItems(any(), anyBoolean())).thenReturn(new ArrayList<>());
		when(this.getItems.getItems(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, MasterType.BRAND.name()).toLowerCase(), true)).thenReturn(Collections.singletonList(item1));
		doThrow(RuntimeException.class).when(this.client).addList(anyString(), anyList());
		this.redisLoader.loaderMaster();
		assertTrue(true);
	}

	@Test
	public void testLoaderChildrenRuntimeException() {
		Map<String, List<Object>> codeMap = new HashMap<>();
		codeMap.put("1¬BRAND", new ArrayList<>(Set.of("2", "3")));
		Map<String, Map<String, List<Object>>> map = new HashMap<>();
		map.put("BRAND", codeMap);

		when(this.getItems.getAllChildItems(any())).thenReturn(new HashMap<>());
		when(this.getItems.getAllChildItems(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, MasterType.BRAND.name()).toLowerCase())).thenReturn(map);
		doThrow(RuntimeException.class).when(this.client).addList(anyString(), anyList());
		this.redisLoader.loaderMaster();
		assertTrue(true);
	}

	@Test
	public void testLoader() {
		ItemDao item1 = new ItemDao();
		item1.setId("1");
		item1.setValue("json");
		Map<String, List<Object>> codeMap = new HashMap<>();
		codeMap.put("1¬BRAND", new ArrayList<>(Set.of("2", "3")));
		Map<String, Map<String, List<Object>>> map = new HashMap<>();
		map.put("BRAND", codeMap);

		when(this.getItems.getItems(any(), anyBoolean())).thenReturn(new ArrayList<>());
		when(this.getItems.getItems(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, MasterType.BRAND.name()).toLowerCase(), false)).thenReturn(Collections.singletonList(item1));
		when(this.getItems.getItems(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, MasterType.BRAND.name()).toLowerCase(), true)).thenReturn(Collections.singletonList(item1));
		when(this.getItems.getAllChildItems(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, MasterType.BRAND.name()).toLowerCase())).thenReturn(map);
		this.redisLoader.loaderMaster();
		assertTrue(true);
	}
}
