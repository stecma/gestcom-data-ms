package com.globalia.infraestructure;

import com.globalia.HelperTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SqlLoaderTest extends HelperTest {

	@InjectMocks
	private SqlLoader sqlLoader;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testLaunch() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "groupCredentialSql", null);
		this.sqlLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testLoaderNoQuery() {
		init(SqlLoader.class, this.sqlLoader, "groupCredentialSql", null);
		this.sqlLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoader() {
		init(SqlLoader.class, this.sqlLoader, "releaseSql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoaderReload() {
		init(SqlLoader.class, this.sqlLoader, "tradePolicySql", HelperTest.QUERIES_EMPTY);
		this.sqlLoader.loader();
		init(SqlLoader.class, this.sqlLoader, "tradePolicySql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoaderReloadEqualsQuery() {
		init(SqlLoader.class, this.sqlLoader, "tradePolicySql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		init(SqlLoader.class, this.sqlLoader, "tradePolicySql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoaderMasterNoQuery() {
		init(SqlLoader.class, this.sqlLoader, "groupCredentialSql", null);
		this.sqlLoader.loaderMaster();
		assertTrue(true);
	}

	@Test
	public void testLoaderMaster() {
		init(SqlLoader.class, this.sqlLoader, "masterSql", HelperTest.QUERIES_MASTER);
		this.sqlLoader.loaderMaster();
		assertTrue(true);
	}

	@Test
	public void testLoaderMasterReload() {
		init(SqlLoader.class, this.sqlLoader, "masterSql", HelperTest.QUERIES_EMPTY);
		this.sqlLoader.loaderMaster();
		init(SqlLoader.class, this.sqlLoader, "masterSql", HelperTest.QUERIES_MASTER);
		this.sqlLoader.loaderMaster();
		assertTrue(true);
	}

	@Test
	public void testLoaderMasterEqualsQuery() {
		init(SqlLoader.class, this.sqlLoader, "masterSql", HelperTest.QUERIES_MASTER);
		this.sqlLoader.loaderMaster();
		init(SqlLoader.class, this.sqlLoader, "masterSql", HelperTest.QUERIES_MASTER);
		this.sqlLoader.loaderMaster();
		assertTrue(true);
	}

	@Test
	public void testGetSqlNoFile() {
		assertNull(this.sqlLoader.getSql(SqlLoader.sqlFile.GROUP.name(), SqlLoader.sqlType.QUERY_ALL.name()));
	}

	@Test
	public void testGetSqlNoQuery() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "customerSql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		TimeUnit.SECONDS.sleep(2);
		assertNull(this.sqlLoader.getSql(SqlLoader.sqlFile.CUSTOMER.name(), "dummy"));
	}

	@Test
	public void testGetSql() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "customerSql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		TimeUnit.SECONDS.sleep(2);
		assertNotNull(this.sqlLoader.getSql(SqlLoader.sqlFile.CUSTOMER.name(), SqlLoader.sqlType.QUERY.name()));
	}

	@Test
	public void testGetChildSqlMapNoQuery() {
		assertTrue(this.sqlLoader.getChildSqlMap(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER.name(), "brand")).isEmpty());
	}

	@Test
	public void testGetChildSqlMap() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "masterSql", HelperTest.QUERIES_MASTER);
		this.sqlLoader.loaderMaster();
		TimeUnit.SECONDS.sleep(2);
		Map<String, String> map = this.sqlLoader.getChildSqlMap(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER.name().toLowerCase(), "brand"));
		assertFalse(map.isEmpty());
		assertEquals(3, map.size());
	}
}
