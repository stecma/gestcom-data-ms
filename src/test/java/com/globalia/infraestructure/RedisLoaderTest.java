package com.globalia.infraestructure;

import com.globalia.HelperTest;
import com.globalia.dto.ItemDao;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.infraestructure.SqlLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class RedisLoaderTest extends HelperTest {

	@InjectMocks
	private RedisLoader redisLoader;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		init(RedisLoader.class, this.redisLoader, "byzonesKey", "key");
		init(RedisLoader.class, this.redisLoader, "tradesKey", "key");
		init(RedisLoader.class, this.redisLoader, "releasesKey", "key");
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testLaunch() throws InterruptedException {
		when(this.getItems.getItems(any())).thenReturn(new ArrayList<>());
		this.redisLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testReload() throws InterruptedException {
		when(this.getItems.getItems(any())).thenReturn(new ArrayList<>());
		this.redisLoader.reload();
		assertTrue(true);
	}

	@Test
	public void testLoaderNoItems() {
		when(this.getItems.getItems(any())).thenReturn(new ArrayList<>());
		this.redisLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoaderRuntimeException() {
		ItemDao item1 = new ItemDao();
		item1.setId("1:1");
		item1.setValue("test");

		ItemDao item2 = new ItemDao();
		item2.setId("1");
		item2.setValue("json");

		when(this.getItems.getItems(any())).thenReturn(new ArrayList<>());
		when(this.getItems.getItems(SqlLoader.sqlFile.TRADE)).thenReturn(List.of(item1));
		when(this.getItems.getItems(SqlLoader.sqlFile.RELEASE)).thenReturn(List.of(item2));
		doThrow(RuntimeException.class).when(this.client).addMap(anyString(), anyMap());
		this.redisLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoader() {
		ItemDao item1 = new ItemDao();
		item1.setId("1:1");
		item1.setValue("test");

		ItemDao item2 = new ItemDao();
		item2.setId("1");
		item2.setValue("json");

		when(this.getItems.getItems(any())).thenReturn(new ArrayList<>());
		when(this.getItems.getItems(SqlLoader.sqlFile.TRADE)).thenReturn(List.of(item1));
		when(this.getItems.getItems(SqlLoader.sqlFile.RELEASE)).thenReturn(List.of(item2));
		this.redisLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testUpdateSupplierByZoneNull() {
		when(this.getItems.getSupplierByZone(any())).thenReturn(null);
		this.redisLoader.updateSupplierByZone("sys", "sap");
		assertTrue(true);
	}

	@Test
	public void testUpdateSupplierByZoneIOException() {
		when(this.getItems.getSupplierByZone(any())).thenReturn(new ItemDao());
		doThrow(RuntimeException.class).when(this.client).add(anyString(), nullable(String.class), nullable(String.class));
		this.redisLoader.updateSupplierByZone("sys¬sub", "sap");
		assertTrue(true);
	}

	@Test
	public void testUpdateSupplierByZone() {
		init(RedisLoader.class, this.redisLoader, "byzonesKey", "key");
		when(this.getItems.getSupplierByZone(any())).thenReturn(new ItemDao());
		this.redisLoader.updateSupplierByZone("sys¬sub", "sap");
		assertTrue(true);
	}

	@Test
	public void testUpdateMasterNull() {
		when(this.getItems.getMaster(any())).thenReturn(null);
		this.redisLoader.updateSystem("sys");
		assertTrue(true);
	}

	@Test
	public void testUpdateMasterIOException() {
		when(this.getItems.getMaster(any())).thenReturn(new ItemDao());
		doThrow(RuntimeException.class).when(this.client).add(anyString(), nullable(String.class), nullable(String.class));
		this.redisLoader.updateSystem("sys¬sub");
		assertTrue(true);
	}

	@Test
	public void testUpdateMaster() {
		init(RedisLoader.class, this.redisLoader, "mastersKey", "key");
		when(this.getItems.getMaster(any())).thenReturn(new ItemDao());
		this.redisLoader.updateSystem("sys¬sub");
		assertTrue(true);
	}
}
