package com.globalia;

import com.globalia.application.repository.GetItems;
import com.globalia.dto.DateRange;
import com.globalia.dto.I18n;
import com.globalia.dto.KafkaItem;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.credential.HotelX;
import com.globalia.enumeration.credential.InfoStatic;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.PercentageType;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisClient;
import com.globalia.application.service.Connector;
import com.globalia.application.service.Group;
import com.globalia.application.repository.SaveTranslate;
import com.globalia.application.service.Supplier;
import org.mockito.Mock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class HelperTest {

    protected static final String QUERIES = "{\"queries\": [ {\"type\": \"query\", \"sql\": \"QUERY\"}, {\"type\": \"query_all\", \"sql\": \"QUERY\"},{\"type\": \"insert\", \"sql\": \"QUERY\"},{\"type\": \"update\", \"sql\": \"QUERY\"}, {\"type\": \"delete\", \"sql\": \"QUERY\"}, { \"type\": \"insert¬connector¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"update¬connector¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"delete¬connector¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"insert¬group¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"update¬group¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"delete¬group¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"insert¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"update¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"delete¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"insert¬credential\", \"sql\": \"QUERY\" }, { \"type\": \"update¬credential\", \"sql\": \"QUERY\" }, { \"type\": \"delete¬credential\", \"sql\": \"QUERY\" }, { \"type\": \"assign¬credential\", \"sql\": \"QUERY\" }]}";
    protected static final String QUERIES_MASTER = "{ \"entities\": [ { \"entity\": \"brand\", \"queries\": [ { \"type\": \"query\", \"sql\": \"QUERY\" }, { \"type\": \"query_all\", \"sql\": \"QUERY\" }, { \"type\": \"insert\", \"sql\": \"QUERY\"}, { \"type\": \"update\", \"sql\": \"QUERY\" }, { \"type\": \"delete\", \"sql\": \"QUERY\" } ], \"children\": [ { \"type\": \"depth¬query\", \"sql\": \"QUERY\" }, { \"type\": \"depth¬insert\", \"sql\": \"QUERY\" }, { \"type\": \"depth¬delete\", \"sql\": \"QUERY\" } ] }, { \"entity\": \"depth\", \"queries\": [ { \"type\": \"query\", \"sql\": \"QUERY\" }, { \"type\": \"query_all\", \"sql\": \"QUERY\" }, { \"type\": \"insert\", \"sql\": \"QUERY\" }, { \"type\": \"update\", \"sql\": \"QUERY\"}, { \"type\": \"delete\", \"sql\": \"QUERY\" } ] }, { \"entity\": \"external_system\", \"queries\": [ { \"type\": \"query\", \"sql\": \"QUERY\" }, { \"type\": \"query_all\", \"sql\": \"QUERY\" }, { \"type\": \"insert\", \"sql\": \"QUERY\" }, { \"type\": \"update\", \"sql\": \"QUERY\"}, { \"type\": \"delete\", \"sql\": \"QUERY\" } ] } ] }";
    protected static final String QUERIES_EMPTY = "{ \"queries\": [ { \"type\": \"dummy\", \"sql\": \"QUERY\" } ] }";

    @Mock
    protected JdbcTemplate jdbcTemplate;
    @Mock
    protected JsonHandler jsonHandler;
    @Mock
    protected SqlLoader sqlLoader;
    @Mock
    protected Context context;
    @Mock
    protected GetItems getItems;
    @Mock
    protected RedisClient client;
    @Mock
    protected SaveTranslate save;
    @Mock
    protected RedisLoader redisLoader;
    @Mock
    protected Dates dates;
    @Mock
    protected Supplier supplier;
    @Mock
    protected Group group;
    @Mock
    protected Connector connector;

    public static KafkaItem message(final String entity) {
        KafkaItem kafka = new KafkaItem();
        kafka.setEntity(entity);
        kafka.setAction("test");
        kafka.setJson("json");

        return kafka;
    }

    public static CancelCostItem cancelCost() {
        CancelCostItem cancel = new CancelCostItem();
        cancel.setId("1¬1¬1¬1¬1¬1¬1¬1¬1¬");
        cancel.setCredential("1");

        return cancel;
    }

    public static SupplierByZoneItem supplierByZone() {
        SupplierByZoneItem supplier = new SupplierByZoneItem();
        supplier.setId("1¬1¬1¬1¬1¬1¬1¬1¬1¬");
        supplier.setCodsis("1");
        supplier.setCodpro("1");
        supplier.setRefzge("1");
        supplier.setCodpai("1");
        supplier.setCodepr("1");
        supplier.setCodare("");
        supplier.setSubprv("1");
        supplier.setCodprvext("1");
        supplier.setCoduser("test");

        return supplier;
    }

    public static ExternalCredentialItem connector() {
        MasterItem master = master(MasterType.BRAND, "1");

        ExternalCredentialItem newExt = new ExternalCredentialItem();
        newExt.setCorporation(master);
        newExt.setInvoicingType(master);
        newExt.setSupplierId("1");
        newExt.setGroupId("1");
        newExt.setId("1");
        newExt.setCheckIn(new DateRange());
        newExt.getCheckIn().setFrom("21/12/2019");
        newExt.getCheckIn().setTo("21/12/2019");
        newExt.setBook(new DateRange());
        newExt.getBook().setFrom("21/12/2019");
        newExt.getBook().setTo("21/12/2019");
        newExt.getBook().setTimeFrom("00:00:00");
        newExt.getBook().setTimeTo("00:00:00");
        newExt.setCodes(new HashSet<>(Set.of(pairValue(MasterType.CORPORATION, "1"), pairValue(MasterType.BILLING_TYPE, "2"))));

        return newExt;
    }

    public static CredentialItem credential() {
        PairValue brd = pairValue(MasterType.BRAND, "1");
        PairValue srvc = pairValue(MasterType.SERVICE_TYPE, "1");
        PairValue channelc = pairValue(MasterType.SALES_CHANNEL, "1");
        PairValue modelc = pairValue(MasterType.DISTRIBUTION_MODEL, "1");
        PairValue rqc = pairValue(MasterType.REQUEST, "1");
        PairValue numcl = pairValue(MasterType.NUM_CLIENTS, "1");
        PairValue envc = pairValue(MasterType.ENVIRONMENTS, "1");
        PairValue rates = pairValue(MasterType.RATE_TYPE, "1");
        PairValue cous = pairValue(MasterType.COUNTRY, "1");
        PairValue depth = pairValue(MasterType.DEPTH, "1");
        PairValue sales = pairValue(MasterType.SALES_MODEL, "1");
        PairValue lang = pairValue(MasterType.LANGUAGES, "1");
        PairValue platform = pairValue(MasterType.PLATFORM, "1");
        PairValue prd = pairValue(MasterType.PRODUCT_ORIGIN, "1");
        PairValue client = pairValue(MasterType.AGENCY, "1");
        PairValue userc = pairValue(MasterType.USERS, "2");
        PairValue currency = pairValue(MasterType.CURRENCY, "2");

        MasterItem master = master(MasterType.AGENCY, "2");
        master.setCodes(Set.of(depth, modelc));

        CredentialItem credential = new CredentialItem();
        credential.setId("test");
        credential.setAlias("test");
        credential.setDescription("test");
        credential.setEnvironment(master);
        credential.setBrand(master);
        credential.setServiceType(master);
        credential.setSaleChannel(master);
        credential.setDistributionModel(master);
        credential.setMarket(master);
        credential.setDepth(master);
        credential.setSalesModel(master);
        credential.setLanguage(master);
        credential.setRequest(master);
        credential.setUser(master);
        credential.setCurrency(master);
        credential.setPlatforms(new HashSet<>(Set.of(master)));
        credential.setRates(new HashSet<>(Set.of(master)));
        credential.setProducts(new HashSet<>(Set.of(master)));
        credential.setCodes(new HashSet<>(Set.of(currency, brd, srvc, channelc, modelc, rqc, numcl, envc, rates, cous, depth, sales, lang, platform, prd, client, userc)));

        return credential;
    }

    public static CustomerItem customer() {
        CustomerItem customer = new CustomerItem();
        customer.setId("1");
        customer.setCredential("1");
        customer.setMasters(Set.of(master(MasterType.AGENCY, "1")));
        customer.setCodes(new HashSet<>(Set.of(pairValue(MasterType.AGENCY, "1"))));
        return customer;
    }

    public static GroupCredentialItem group() {
        MasterItem master = new MasterItem();
        // Credenciales externas
        ExternalCredentialItem newExt = new ExternalCredentialItem();
        newExt.setCreate(true);
        newExt.setCorporation(master);
        newExt.setInvoicingType(master);
        newExt.setCodes(new HashSet<>());
        newExt.getCodes().add(pairValue(MasterType.CORPORATION, "1"));
        newExt.getCodes().add(pairValue(MasterType.BILLING_TYPE, "2"));

        ExternalCredentialItem updExt = new ExternalCredentialItem();
        updExt.setCorporation(master);
        updExt.setInvoicingType(master);

        ExternalCredentialItem delExt = new ExternalCredentialItem();
        delExt.setDeleted(true);
        delExt.setCorporation(master);
        delExt.setInvoicingType(master);

        GroupCredentialItem newGroup = new GroupCredentialItem();
        newGroup.setRates(Set.of(master));
        newGroup.setCurrencies(Set.of(master));
        newGroup.setDistriModels(Set.of(master));
        newGroup.setCountries(Set.of(master));
        newGroup.setExcludedCountries(Set.of(master));
        newGroup.setClientType(master);
        newGroup.setBrand(master);
        newGroup.setHotelX(HotelX.NO);
        newGroup.setCodes(new HashSet<>());
        newGroup.getCodes().add(pairValue(MasterType.COUNTRY, "1"));
        newGroup.getCodes().add(pairValue(MasterType.RATE_TYPE, "1"));
        newGroup.getCodes().add(pairValue(MasterType.DISTRIBUTION_MODEL, "1"));
        newGroup.getCodes().add(pairValue(MasterType.CURRENCY, "1"));
        newGroup.getCodes().add(pairValue(MasterType.BRAND, "1"));
        newGroup.getCodes().add(pairValue(MasterType.CLIENT_TYPE, "1"));
        newGroup.setExternalConfig(Set.of(newExt, updExt, delExt));
        return newGroup;
    }

    public static TradePolicyItem trade() {
        TradePolicyItem trade = new TradePolicyItem();
        trade.setId("1¬1¬1¬1¬1¬1¬1¬1¬1¬");
        trade.setCredential("1");
        trade.setPercentage(1D);
        trade.setPercentageType(PercentageType.MARKUP);
        trade.setCodes(new HashSet<>(Set.of(pairValue(MasterType.SERVICE_TYPE, "1"))));
        trade.setMasters(new HashSet<>(Set.of(master(MasterType.SERVICE_TYPE, "1"))));

        return trade;
    }

    public static CurrencyItem currency() {
        CurrencyItem currency = new CurrencyItem();
        currency.setId("1");
        currency.setCredential("1");
        currency.setMasters(Set.of(master(MasterType.COUNTRY, "1")));
        currency.setCodes(new HashSet<>(Set.of(pairValue(MasterType.COUNTRY, "1"))));
        return currency;
    }

    public static SupplierItem supplier() {
        MasterItem master = new MasterItem();
        // Grupos de credenciales
        GroupCredentialItem updGroup = new GroupCredentialItem();
        updGroup.setRates(Set.of(master));
        updGroup.setCurrencies(Set.of(master));
        updGroup.setDistriModels(Set.of(master));
        updGroup.setCountries(Set.of(master));

        GroupCredentialItem delGroup = new GroupCredentialItem();
        delGroup.setDeleted(true);
        delGroup.setRates(Set.of(master));
        delGroup.setCurrencies(Set.of(master));
        delGroup.setDistriModels(Set.of(master));
        delGroup.setCountries(Set.of(master));

        MasterItem filter = new MasterItem();
        filter.setId("1¬1");
        filter.setOwnProduct(true);
        filter.setSapCode("test");

        SupplierItem supplier = new SupplierItem();
        supplier.setId("1");
        supplier.setStatus(SupplierStatus.DISABLED);
        supplier.setCodes(new HashSet<>());
        supplier.getCodes().add(pairValue(MasterType.SERVICE_TYPE, "1"));
        supplier.getCodes().add(pairValue(MasterType.EXTERNAL_SYSTEM, "1¬1"));
        supplier.getCodes().add(pairValue(MasterType.USERS, "1"));
        supplier.getCodes().add(pairValue(MasterType.CLIENT_PREF, "1"));
        supplier.getCodes().add(pairValue(MasterType.AGENCY, new LinkedHashSet<>(Set.of("1", "2"))));
        supplier.getCodes().add(pairValue(MasterType.TOP_DESTINATION, "1"));
        supplier.setBuyModel(filter);
        supplier.setClientPref(filter);
        supplier.setUser(filter);
        supplier.setServiceType(filter);
        supplier.setSystem(filter);
        supplier.setClients(Set.of(filter));
        supplier.setTopDestinations(Set.of(filter));
        supplier.setExternalSys(Set.of(group(), updGroup, delGroup));
        supplier.setStaticInfo(InfoStatic.OFFLINE);
        supplier.setStatus(SupplierStatus.DISABLED);
        supplier.setActive(true);
        return supplier;
    }

    public static ReleaseItem release() {
        ReleaseItem release = new ReleaseItem();
        release.setId("1");
        release.setCodes(new HashSet<>(Set.of(pairValue(MasterType.BRAND, "1"))));
        release.setMasters(new HashSet<>(Set.of(master(MasterType.BRAND, "1"))));
        release.setCredential("1");
        release.setDays(1);
        release.setOperationDays("X");

        return release;
    }

    public static MasterItem master() {
        I18n i18nEN = new I18n();
        i18nEN.setKey("ENG");
        i18nEN.setValue("TEST");

        I18n i18nES = new I18n();
        i18nES.setKey("ESP");
        i18nES.setValue("TEST");

        MasterItem master = new MasterItem();
        master.setId("1");
        master.setNames(new LinkedHashSet<>(Set.of(i18nEN, i18nES)));
        master.setEntity(MasterType.BRAND.name());
        master.setMasters(Set.of(master(MasterType.DEPTH, "1")));
        master.setCodes(Set.of(pairValue(MasterType.DEPTH, "1"), pairValue(MasterType.SERVICE_TYPE, "1")));
        return master;
    }

    protected static PairValue pairValue(final MasterType type, final String value) {
        return pairValue(type, new LinkedHashSet<>(Set.of(value)));
    }

    protected static PairValue pairValue(final MasterType type, final LinkedHashSet<String> values) {
        PairValue pair = new PairValue();
        pair.setKey(type.name());
        pair.setValue(values);
        return pair;
    }

    public static MasterItem master(final MasterType type, final String value) {
        MasterItem child = new MasterItem();
        child.setId(value);
        child.setNames(new LinkedHashSet<>());
        child.setEntity(type.name());
        return child;
    }

    public static void defaultLanguage(final Class<?> classes, final Object obj) {
        try {
            Field field = classes.getDeclaredField("defaultLanguage");
            field.setAccessible(true);
            field.set(obj, "ESP");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void init(final Class<?> classes, final Object obj, final String fieldName, final String fieldValue) {
        if (StringUtils.hasText(fieldName)) {
            try {
                Field field = classes.getDeclaredField(fieldName);
                field.setAccessible(true);
                field.set(obj, fieldValue);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}