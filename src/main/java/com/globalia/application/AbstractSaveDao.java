package com.globalia.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.dao.SaveDao;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.LogType;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import com.globalia.application.service.Connector;
import com.globalia.application.service.Group;
import com.globalia.application.service.Supplier;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Set;

@Slf4j
public abstract class AbstractSaveDao<T> extends SaveDao<T> {

	public static final String EXCEPTION_SER_MSG = "Unknown error during object serialization %s";
	public static final String EXCEPTION_DES_MSG = "Unknown error during object deserialization %s";
	protected static final String CRE_ACTION = "create";
	protected static final String UPD_ACTION = "update";
	protected static final String QUERY_NOT_FOUND = "Query not found %s";

	@Getter
	@Value("${logsFormat}")
	private String logsFormat;

	@Getter
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Getter
	@Autowired
	private SqlLoader sqlLoader;
	@Autowired
	private Connector connector;
	@Autowired
	private Group group;
	@Autowired
	private Supplier supplier;

	protected abstract Object[] getParams(final SqlLoader.sqlType sqlType, final T item) throws JsonProcessingException;

	protected abstract String getSqlFile(final T item);

	protected abstract T getObject(String json) throws IOException;

	protected abstract void setResult(final ItemResponse response, final T item, final SqlLoader.sqlType sqlType);

	public ItemResponse sendToBBDD(final String action, final String json) {
		ItemResponse response = new ItemResponse();
		try {
			switch (action) {
				case CRE_ACTION:
					return this.createItem(this.getObject(json));
				case UPD_ACTION:
					return this.updateItem(this.getObject(json));
				default:
					return this.deleteItem(this.getObject(json));
			}
		} catch (IOException e) {
			AbstractSaveDao.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_DES_MSG, e.getMessage()));
			response.setError(new Error());
		}
		return response;
	}

	public void translate(final SqlLoader.sqlFile file, final T item) {
		switch (file) {
			case CONNECTOR:
				this.connector.translateExternal((ExternalCredentialItem) item);
				break;
			case GROUP:
				this.group.translateGroup((GroupCredentialItem) item);
				break;
			default:
				this.supplier.translateSupplier((SupplierItem) item);
		}
	}

	public ItemResponse createItem(final T item) {
		return this.execute(SqlLoader.sqlType.INSERT, item);
	}

	public ItemResponse updateItem(final T item) {
		return this.execute(SqlLoader.sqlType.UPDATE, item);
	}

	public ItemResponse deleteItem(final T item) {
		return this.execute(SqlLoader.sqlType.DELETE, item);
	}

	protected ItemResponse execute(final SqlLoader.sqlType sqlType, final T item) {
		ItemResponse response = new ItemResponse();
		try {
			response.setError(this.executeDB(getSqlFile(item), sqlType.name(), getParams(sqlType, item)));
			if (response.getError() == null) {
				this.setResult(response, item, sqlType);
			}
		} catch (JsonProcessingException jpe) {
			AbstractSaveDao.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_SER_MSG, jpe.getMessage()));
			response.setError(new Error());
		}
		return response;
	}

	protected Error executeDB(final String file, final String sqlType, final Object[] params) {
		try {
			String query = this.sqlLoader.getSql(file, sqlType);
			if (StringUtils.hasText(query)) {
				this.jdbcTemplate.update(query, params);
				return null;
			}
			AbstractSaveDao.log.error(this.logsFormat, getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(QUERY_NOT_FOUND, sqlType));
		} catch (RuntimeException data) {
			AbstractSaveDao.log.error(this.logsFormat, getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_SER_MSG, data.getMessage()));
		}
		return new Error();
	}

	protected Object[] getParams(final SqlLoader.sqlType sqlType, final String credential, final String code, final String json) {
		switch (sqlType) {
			case DELETE:
				return new Object[]{credential, code};
			case UPDATE:
				return new Object[]{json, credential, code};
			default:
				return new Object[]{credential, code, json};
		}
	}

	protected String getCode(final Set<PairValue> codes, final MasterType entity) {
		String code = null;
		for (PairValue pair : codes) {
			if (pair.getKey().equals(entity.name())) {
				code = pair.getValue().iterator().next();
				break;
			}
		}
		return code;
	}
}