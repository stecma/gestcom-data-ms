package com.globalia.application.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.Context;
import com.globalia.application.repository.SaveCredentialAliasCount;
import com.globalia.application.repository.SaveCredentialIndexCount;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.application.repository.SaveCancelCost;
import com.globalia.application.repository.SaveConnector;
import com.globalia.application.repository.SaveCredential;
import com.globalia.application.repository.SaveCurrency;
import com.globalia.application.repository.SaveCurrencyByCountry;
import com.globalia.application.repository.SaveCustomer;
import com.globalia.dto.KafkaItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.LogType;
import com.globalia.application.repository.SaveAssignGroup;
import com.globalia.application.repository.SaveGroup;
import com.globalia.application.Health;
import com.globalia.json.JsonHandler;
import com.globalia.application.repository.SaveMaster;
import com.globalia.application.repository.SaveRelease;
import com.globalia.application.repository.SaveSupplier;
import com.globalia.application.repository.SaveSupplierByZone;
import com.globalia.application.repository.SaveTrade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Service
@RefreshScope
@Slf4j
public class MessageConsumerReply {

	@Value("${logsFormat}")
	private String logsFormat;

	@Autowired
	private Context context;
	@Autowired
	private JsonHandler jsonHandler;
	@Autowired
	private Health health;
	@Autowired
	private RedisLoader redis;
	@Autowired
	private SaveCancelCost cancelCost;
	@Autowired
	private SaveConnector connector;
	@Autowired
	private SaveCredential credential;
	@Autowired
	private SaveCustomer customer;
	@Autowired
	private SaveCurrency currency;
	@Autowired
	private SaveCurrencyByCountry currencyByCountry;
	@Autowired
	private SaveMaster master;
	@Autowired
	private SaveSupplier supplier;
	@Autowired
	private SaveRelease release;
	@Autowired
	private SaveTrade trade;
	@Autowired
	private SaveSupplierByZone supplierbyzone;
	@Autowired
	private SaveAssignGroup assign;
	@Autowired
	private SaveGroup group;
	@Autowired
	private SaveCredentialIndexCount saveCredentialIndex;
	@Autowired
	private SaveCredentialAliasCount saveCredentialAlias;

	@KafkaListener(topics = "${kafka.topicname}")
	@SendTo("${kafka.topicreplyname}")
	public String processMessage(final String message) throws InterruptedException {
		MessageConsumerReply.log.info(String.format("Received content: %s", message));
		try {
			KafkaItem item = (KafkaItem) this.jsonHandler.fromJson(message, KafkaItem.class);
			switch (item.getEntity()) {
				case "cancelcost":
					return this.cancelCostProcess(item);
				case "connector":
					return this.connectorProcess(item);
				case "credential":
					return this.credentialProcess(item);
				case "customer":
					return this.customerProcess(item);
				case "currency":
					return this.currencyProcess(item, false);
				case "currencymaster":
					return this.currencyProcess(item, true);
				case "release":
					return this.releaseProcess(item);
				case "trade":
					return this.tradeProcess(item);
				case "assign":
					return this.assignProcess(item);
				case "group":
					return this.groupProcess(item);
				case "supplier":
					return this.supplierProcess(item);
				case "supplierbyzone":
					return this.supplierByZoneProcess(item);
				case "addsapitem":
					return this.update(item.getJson(), true);
				case "addserviceitem":
					return this.update(item.getJson(), false);
				case "master":
					return this.masterProcess(item);
				case "credentialIndex":
					return this.credentialIndexProcess(item);
				case "credentialAlias":
					return this.credentialAliasProcess(item);
				case "reload":
					this.redis.reload();
					return "OK";
				default:
					return this.healthProcess(item);
			}
		} catch (IOException e) {
			MessageConsumerReply.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(AbstractSaveDao.EXCEPTION_SER_MSG, e.getMessage()));
		}
		return null;
	}

	private String credentialAliasProcess(KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.saveCredentialAlias.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getCredentialAliasCounter());
	}

	private String credentialIndexProcess(KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.saveCredentialIndex.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getCredentialIndexCounter());
	}

	private String healthProcess(final KafkaItem item) throws JsonProcessingException {
		item.setJson(this.jsonHandler.toJson(this.health.health()));
		return this.jsonHandler.toJson(item);
	}

	private String cancelCostProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.cancelCost.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getCancelCost());
	}

	private String connectorProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.connector.sendToBBDD(item.getAction(), item.getJson());
		if (response.getError() == null) {
			this.connector.translate(SqlLoader.sqlFile.CONNECTOR, response.getExternal());
		}
		return this.jsonHandler.toJson(response.getExternal());
	}

	private String credentialProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.credential.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getCredential());
	}

	private String currencyProcess(final KafkaItem item, final boolean isMaster) throws JsonProcessingException {
		ItemResponse response;
		if (!isMaster) {
			response = this.currency.sendToBBDD(item.getAction(), item.getJson());
		} else {
			response = this.currencyByCountry.sendToBBDD(item.getAction(), item.getJson());
		}
		return this.jsonHandler.toJson(response.getCurrency());
	}

	private String customerProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.customer.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getCustomer());
	}

	private String masterProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.master.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getMaster());
	}

	private String releaseProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.release.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getRelease());
	}

	private String tradeProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.trade.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getTradePolicy());
	}

	private String groupProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.group.sendToBBDD(item.getAction(), item.getJson());
		if (response.getError() == null) {
			this.group.translate(SqlLoader.sqlFile.GROUP, response.getGroup());
		}
		return this.jsonHandler.toJson(response.getGroup());
	}

	private String assignProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.assign.assign(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getGroup());
	}

	private String supplierProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.supplier.sendToBBDD(item.getAction(), item.getJson());
		if (response.getError() == null) {
			this.supplier.translate(SqlLoader.sqlFile.SUPPLIER, response.getSupplier());
		}
		return this.jsonHandler.toJson(response.getSupplier());
	}

	private String supplierByZoneProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.supplierbyzone.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getSupplierByZone());
	}

	private String update(final String json, final boolean isSap) {
		if (StringUtils.hasText(json)) {
			String[] keys = json.split("#");
			if (keys.length == 3) {
				if (isSap) {
					this.supplier.updateSap(keys[0], keys[1], Boolean.parseBoolean(keys[2]));
					this.redis.updateSupplierByZone(keys[0], keys[1]);
				} else {
					this.supplier.updateService(keys[0], keys[1], keys[2]);
				}
				this.redis.updateSystem(keys[0]);
			}
		}
		return "OK";
	}
}