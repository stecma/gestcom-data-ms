package com.globalia.application.service;

import com.globalia.application.Translate;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.LogType;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.json.JsonHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Slf4j
@Component
public class Group extends Translate {

	@Autowired
	private JsonHandler jsonHandler;

	public void translateGroup(final GroupCredentialItem group) {
		Object[] params = null;
		String sqlType = "delete¬group¬supplier";
		if (group.isDeleted()) {
			params = new Object[]{group.getId()};
		} else {
			String system = (String) getClient().get(String.format(RedisLoader.REDIS_KEY_FORMAT, getRedisLoader().getRediskey(translateKey, false), group.getSupplierId()), "SYSTEM");
			String subSystem = (String) getClient().get(String.format(RedisLoader.REDIS_KEY_FORMAT, getRedisLoader().getRediskey(translateKey, false), group.getSupplierId()), "SUBSYSTEM");
			String json = (String) getClient().get(String.format(RedisLoader.REDIS_KEY_FORMAT, getRedisLoader().getRediskey(translateKey, false), group.getSupplierId()), MasterType.SERVICE_TYPE.name());
			if (StringUtils.hasText(json)) {
				MasterItem serviceType = this.getMaster(json);
				if (!group.isCreate()) {
					sqlType = "update¬group¬supplier";
					params = this.getParams(null, system, subSystem, group, serviceType);
				} else {
					sqlType = "insert¬group¬supplier";
					params = this.getParams(getSave().getGroupId(), system, subSystem, group, serviceType);
				}
			}
		}

		getSave().translateGroup(sqlType, params, getContext().getEnvironment());
	}

	private Object[] getParams(final Long seq, final String system, final String subSystem, final GroupCredentialItem group, final MasterItem serviceType) {
		String dft = switchValue(group.isByDefault());
		String active = switchValue(group.isActive());
		String test = switchValue(group.isTest());
		String hotX = group.getHotelX().name().substring(0, 1);
		String brand = getCode(getPairValues(MasterType.BRAND.name(), group.getCodes()));
		String clientType = getCode(getPairValues(MasterType.CLIENT_TYPE.name(), group.getCodes()));

		String market = null;
		String marketExc = null;
		if (!group.isTagNationality()) {
			market = getCodes(getPairValues(MasterType.COUNTRY.name(), group.getCodes()));
			marketExc = getCodes(getPairValues(String.format("%s¬exc", MasterType.COUNTRY.name()), group.getCodes()));
		}
		String currency = null;
		if (!group.isTagCurrency()) {
			currency = getCodes(getPairValues(MasterType.CURRENCY.name(), group.getCodes()));
		}
		String rates = getValues(group.getAllMasters(), group.getCodes(), MasterType.RATE_TYPE.name(), serviceType != null ? serviceType.getCodes() : null);

		MasterItem client = null;
		String json = (String) getClient().get(getRedisLoader().getRediskey("gescom:masters:CLIENT_TYPE", false), clientType);
		if (StringUtils.hasText(json)) {
			client = this.getMaster(json);
		}
		String models = getValues(group.getAllMasters(), group.getCodes(), MasterType.DISTRIBUTION_MODEL.name(), client != null ? client.getCodes() : null);

		if (seq == null) {
			return new Object[]{group.getSysName(), dft, active, test, hotX, brand, clientType, rates, models, marketExc, market, currency, group.getId()};
		}
		getClient().add(String.format(RedisLoader.REDIS_KEY_FORMAT, getRedisLoader().getRediskey(translateKey, false), group.getId()), "SEQUENCE", seq.toString());
		return new Object[]{seq, system, subSystem, group.getSysName(), dft, active, test, hotX, brand, clientType, rates, models, marketExc, market, currency, group.getId()};
	}

	private MasterItem getMaster(final String json) {
		try {
			return (MasterItem) this.jsonHandler.fromJson(json, MasterItem.class);
		} catch (IOException e) {
			Group.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format("Error: %s", e.getLocalizedMessage()));
		}
		return null;
	}
}