package com.globalia.application.service;

import com.globalia.application.Translate;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.enumeration.credential.MasterType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;

@Component
public class Connector extends Translate {

	private final SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy");
	private final SimpleDateFormat formatterTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public void translateExternal(final ExternalCredentialItem external) {
		Object[] params;
		String sqlType;
		if (external.isDeleted()) {
			sqlType = "delete¬connector¬supplier";
			params = new Object[]{external.getId()};
		} else {
			String group = (String) getClient().get(String.format(RedisLoader.REDIS_KEY_FORMAT, getRedisLoader().getRediskey(translateKey, false), external.getGroupId()), "SEQUENCE");
			Long idGroup = null;
			if (StringUtils.hasText(group)) {
				idGroup = Long.valueOf(group);
			}

			if (!external.isCreate()) {
				sqlType = "update¬connector¬supplier";
			} else {
				sqlType = "insert¬connector¬supplier";
			}
			params = this.getParams(idGroup, external);
		}
		getSave().translateExternal(sqlType, params, getContext().getEnvironment());
	}

	private Object[] getParams(final Long idGroup, final ExternalCredentialItem external) {
		String active = this.switchValue(external.isActive());
		String chkFrom = this.formatterDate.format(getDate(external.getCheckIn().getFrom(), null, "01/01/2020", "00:00:00"));
		String chkTo = this.formatterDate.format(getDate(external.getCheckIn().getTo(), null, "31/12/2050", "23:59:59"));
		String bookFrom = this.formatterTime.format(getDate(external.getBook().getFrom(), external.getBook().getTimeFrom(), "01/01/2020", "00:00:00"));
		String bookTo = this.formatterTime.format(getDate(external.getBook().getTo(), external.getBook().getTimeTo(), "31/12/2050", "23:59:59"));

		String corp = getCode(getPairValues(MasterType.CORPORATION.name(), external.getCodes()));
		String invoice = getCode(getPairValues(MasterType.BILLING_TYPE.name(), external.getCodes()));

		if (idGroup == null) {
			return new Object[]{corp, external.getUser(), external.getPass(), external.getConnector(), invoice, active, chkFrom, chkTo, bookFrom, bookTo, external.getId()};
		}
		return new Object[]{idGroup, corp, external.getUser(), external.getPass(), external.getConnector(), invoice, active, chkFrom, chkTo, bookFrom, bookTo, external.getId()};
	}
}