package com.globalia.application.service;

import com.globalia.application.Translate;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.LogType;
import com.globalia.enumeration.credential.InfoStatic;
import com.globalia.enumeration.credential.MasterType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Slf4j
@Component
public class Supplier extends Translate {

	public void translateSupplier(final SupplierItem item) {
		Object[] params = null;
		String sqlType;
		if (item.isDeleted()) {
			sqlType = "delete¬supplier";
			params = new Object[]{item.getId()};
		} else {
			sqlType = "update¬supplier";

			String system = getCode(getPairValues(MasterType.EXTERNAL_SYSTEM.name(), item.getCodes()));
			if (!StringUtils.hasText(system)) {
				Supplier.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, null, null, null, "Empty system");
			} else {
				String subSystem = null;
				if (system.contains("¬")) {
					String[] keys = system.split("¬");
					system = keys[0];
					subSystem = keys[1];
				}
				params = this.getParams(system, subSystem, item);
			}
		}

		if (params != null) {
			getSave().translateSupplier(sqlType, params, getContext().getEnvironment());
		}
	}

	private Object[] getParams(final String system, final String subSystem, final SupplierItem item) {
		String active = switchValue(item.isActive());
		String xsell = switchValue(item.isXsell());
		String mhr = switchValue(item.isMultiHotel());
		String timeout = switchValue(item.isTimeout());
		String test = switchValue(item.isTest());
		String block = switchValue(item.isBlock());
		String availCst = switchValue(item.isAvailCost());
		String disact = switchValue(item.isDiscrepancyStatus());
		String disext = switchValue(item.isDiscrepancyExtended());
		String renta = switchValue(item.isEffectiveCost());
		String regimen = switchValue(item.isGeneralRegimen());
		String sla = switchValue(item.isHasSla());
		String stcinf = item.getStaticInfo() == InfoStatic.OFFLINE ? "F" : item.getStaticInfo().name().substring(0, 1);
		String status = item.getStatus().name().substring(0, 1);

		String clients = getCodes(getPairValues(MasterType.AGENCY.name(), item.getCodes()));
		String topdst = getCodes(getPairValues(MasterType.TOP_DESTINATION.name(), item.getCodes()));
		String clientPref = getCode(getPairValues(MasterType.CLIENT_PREF.name(), item.getCodes()));
		String srvType = getCode(getPairValues(MasterType.SERVICE_TYPE.name(), item.getCodes()));
		String kam = getCode(getPairValues(MasterType.USERS.name(), item.getCodes()));
		String buyType = getCode(getPairValues(MasterType.BUY_MODEL.name(), item.getCodes()));
		String buyModel = "X";
		if ("N".equalsIgnoreCase(buyType)) {
			buyModel = "C";
		} else if ("C".equalsIgnoreCase(buyType)) {
			buyModel = "P";
		}

		// Añadir valores en la redis
		getClient().add(String.format(RedisLoader.REDIS_KEY_FORMAT, getRedisLoader().getRediskey(translateKey, false), item.getId()), "SYSTEM", system);
		if (StringUtils.hasText(subSystem)) {
			getClient().add(String.format(RedisLoader.REDIS_KEY_FORMAT, getRedisLoader().getRediskey(translateKey, false), item.getId()), "SUBSYSTEM", subSystem);
		}
		getClient().add(String.format(RedisLoader.REDIS_KEY_FORMAT, getRedisLoader().getRediskey(translateKey, false), item.getId()), MasterType.SERVICE_TYPE.name(), getClient().get(getRedisLoader().getRediskey("gescom:masters:SERVICE_TYPE", false), srvType));

		return new Object[]{active, status, xsell, clients, item.getGiata(), mhr, timeout, test, stcinf, block, availCst, disact, disext, renta, regimen, sla, item.getHotelNum(), item.getRoomMax(), item.getThreadMax(), buyModel, clientPref, srvType, kam, topdst, system, subSystem};
	}
}