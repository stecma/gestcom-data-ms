package com.globalia.application;

import com.globalia.Context;
import com.globalia.Dates;
import com.globalia.application.repository.SaveTranslate;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.dto.PairValue;
import com.globalia.redis.RedisClient;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@RefreshScope
@Component
public class Translate {

	@Value("${redis.translateKey}")
	protected String translateKey;
	@Getter
	@Value("${logsFormat}")
	private String logsFormat;

	@Getter
	@Autowired
	private SaveTranslate save;
	@Getter
	@Autowired
	private Context context;
	@Getter
	@Autowired
	private RedisLoader redisLoader;
	@Getter
	@Autowired
	private RedisClient client;
	@Autowired
	private Dates dates;

	protected Date getDate(final String date, final String time, final String dateDft, final String timeDft) {
		return this.dates.getDateTime(StringUtils.hasText(date) ? date : dateDft, StringUtils.hasText(time) ? time : timeDft, null);
	}

	protected String getCode(final Set<String> codes) {
		return !codes.isEmpty() ? codes.iterator().next() : null;
	}

	protected String getCodes(final Set<String> codes) {
		if (!codes.isEmpty()) {
			StringBuilder builder = new StringBuilder();
			for (String code : codes) {
				if (builder.length() > 1) {
					builder.append(',');
				}
				builder.append('"').append(code).append('"');
			}
			return '[' + builder.toString() + ']';
		}
		return null;
	}

	protected String getValues(final Set<String> allMasters, final Set<PairValue> codes, final String masterType, final Set<PairValue> otherCodes) {
		return this.getCodes(this.getPairValues(masterType, allMasters != null && allMasters.contains(masterType.toLowerCase()) ? otherCodes : codes));
	}

	protected Set<String> getPairValues(final String entity, final Set<PairValue> codes) {
		if (codes != null) {
			for (PairValue pair : codes) {
				if (pair.getKey().equals(entity)) {
					return pair.getValue();
				}
			}
		}
		return new HashSet<>();
	}

	protected String switchValue(final boolean switchValue) {
		return switchValue ? "S" : "N";
	}
}