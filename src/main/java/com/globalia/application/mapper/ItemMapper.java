package com.globalia.application.mapper;

import com.globalia.dto.ItemDao;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemMapper implements RowMapper<ItemDao> {

	public ItemDao mapRow(final ResultSet resultSet, final int index) throws SQLException {
		ItemDao item = new ItemDao();
		item.setId(resultSet.getString("id"));
		item.setValue(resultSet.getString("json"));
		item.setUpdateDate(resultSet.getString("updated"));
		return item;
	}
}