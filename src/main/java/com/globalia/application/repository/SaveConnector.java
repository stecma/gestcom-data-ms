package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MasterType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveConnector extends AbstractSaveDao<ExternalCredentialItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final ExternalCredentialItem item) throws JsonProcessingException {
		switch (sqlType) {
			case DELETE:
				return new Object[]{getJsonHandler().toJson(item), item.getGroupId(), item.getSupplierId(), item.getId()};
			case UPDATE:
				return new Object[]{getCode(item.getCodes(), MasterType.CORPORATION), getCode(item.getCodes(), MasterType.BILLING_TYPE), getJsonHandler().toJson(item), item.getGroupId(), item.getSupplierId(), item.getId()};
			default:
				return new Object[]{item.getId(), item.getGroupId(), item.getSupplierId(), getCode(item.getCodes(), MasterType.CORPORATION), getCode(item.getCodes(), MasterType.BILLING_TYPE), getJsonHandler().toJson(item)};
		}
	}

	@Override
	protected String getSqlFile(final ExternalCredentialItem item) {
		return SqlLoader.sqlFile.CONNECTOR.name().toLowerCase();
	}

	@Override
	protected ExternalCredentialItem getObject(final String json) throws IOException {
		return (ExternalCredentialItem) getJsonHandler().fromJson(json, ExternalCredentialItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final ExternalCredentialItem item, final SqlLoader.sqlType sqlType) {
		response.setExternal(item);
	}
}