package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.TradePolicyItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveTrade extends AbstractSaveDao<TradePolicyItem> {

	protected Object[] getParams(final SqlLoader.sqlType sqlType, final TradePolicyItem item) throws JsonProcessingException {
		return getParams(sqlType, item.getCredential(), item.getId(), getJsonHandler().toJson(item));
	}

	@Override
	protected String getSqlFile(final TradePolicyItem item) {
		return SqlLoader.sqlFile.TRADE.name().toLowerCase();
	}

	@Override
	protected TradePolicyItem getObject(final String json) throws IOException {
		return (TradePolicyItem) getJsonHandler().fromJson(json, TradePolicyItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final TradePolicyItem item, final SqlLoader.sqlType sqlType) {
		response.setTradePolicy(item);
	}
}