package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveGroup extends AbstractSaveDao<GroupCredentialItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final GroupCredentialItem item) throws JsonProcessingException {
		switch (sqlType) {
			case DELETE:
				return new Object[]{getJsonHandler().toJson(item), item.getId(), item.getSupplierId()};
			case UPDATE:
				return new Object[]{item.getSysName(), item.isActive(), item.isTest(), item.getHotelX().name(), getJsonHandler().toJson(item), item.getId(), item.getSupplierId()};
			default:
				return new Object[]{item.getId(), item.getSupplierId(), item.getSysName(), item.isActive(), item.isTest(), item.getHotelX().name(), getJsonHandler().toJson(item)};
		}
	}

	@Override
	protected String getSqlFile(final GroupCredentialItem item) {
		return SqlLoader.sqlFile.GROUP.name().toLowerCase();
	}

	@Override
	protected GroupCredentialItem getObject(final String json) throws IOException {
		return (GroupCredentialItem) getJsonHandler().fromJson(json, GroupCredentialItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final GroupCredentialItem item, final SqlLoader.sqlType sqlType) {
		response.setGroup(item);
	}
}