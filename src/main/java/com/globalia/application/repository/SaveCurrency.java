package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.dto.credential.ItemResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveCurrency extends AbstractSaveDao<CurrencyItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final CurrencyItem item) throws JsonProcessingException {
		return getParams(sqlType, item.getCredential(), item.getId(), getJsonHandler().toJson(item));
	}

	@Override
	protected String getSqlFile(final CurrencyItem item) {
		return SqlLoader.sqlFile.CURRENCY.name().toLowerCase();
	}

	@Override
	protected CurrencyItem getObject(final String json) throws IOException {
		return (CurrencyItem) getJsonHandler().fromJson(json, CurrencyItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final CurrencyItem item, final SqlLoader.sqlType sqlType) {
		response.setCurrency(item);
	}
}