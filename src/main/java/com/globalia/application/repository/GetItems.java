package com.globalia.application.repository;

import com.globalia.Context;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.ItemDao;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.enumeration.LogType;
import com.globalia.application.mapper.ItemMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Dao
@Component
@Slf4j
public class GetItems {

	@Value("${logsFormat}")
	private String logsFormat;

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private SqlLoader sqlLoader;
	@Autowired
	private Context context;

	public List<ItemDao> getItems(final SqlLoader.sqlFile file) {
		return this.getItems(file.name(), false);
	}

	public List<ItemDao> getItems(final String file, final boolean scanId) {
		SqlLoader.sqlType sqlType = SqlLoader.sqlType.QUERY_ALL_SCAN;
		if (!scanId) {
			sqlType = SqlLoader.sqlType.QUERY_ALL;
		}
		List<ItemDao> items = null;
		String query = this.sqlLoader.getSql(file, sqlType.name());
		if (StringUtils.hasText(query)) {
			items = this.jdbcTemplate.query(query, new ItemMapper());
		}
		return items;
	}

	public ItemDao getSupplierByZone(final SupplierByZoneItem item) {
		return this.getItem(SqlLoader.sqlFile.SUPPLIER_BYZONE.name(), new Object[]{item.getCodsis(), item.getCodpro(), item.getRefzge(), item.getCodpai(), item.getCodepr(), item.getCodare(), item.getSubprv(), item.getCodprvext()});
	}

	public ItemDao getMaster(final MasterItem item) {
		return this.getItem(String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, item.getEntity()).toLowerCase(), new Object[]{item.getId()});
	}

	private ItemDao getItem(final String file, final Object[] params) {
		ItemDao item = null;
		String query = this.sqlLoader.getSql(file, SqlLoader.sqlType.QUERY.name());
		if (StringUtils.hasText(query)) {
			try {
				item = this.jdbcTemplate.queryForObject(query, new ItemMapper(), params);
			} catch (EmptyResultDataAccessException e) {
				GetItems.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), e.getMessage());
			}
		}
		return item;
	}

	public Map<String, Map<String, List<Object>>> getAllChildItems(final String file) {
		Map<String, Map<String, List<Object>>> map = new HashMap<>();
		Map<String, String> queryMap = this.sqlLoader.getChildSqlMap(file);
		for (Map.Entry<String, String> entry : queryMap.entrySet()) {
			String[] keys = entry.getKey().split("¬");
			if (SqlLoader.sqlType.valueOf(keys[1].toUpperCase()) != SqlLoader.sqlType.QUERY) {
				continue;
			}
			this.addChildValues(map, keys[0], this.jdbcTemplate.query(entry.getValue(), new ItemMapper()));
		}
		return map;
	}

	private void addChildValues(final Map<String, Map<String, List<Object>>> map, final String key, final List<ItemDao> list) {
		if (list != null && !list.isEmpty()) {
			for (ItemDao item : list) {
				map.computeIfAbsent(key, k -> new HashMap<>());
				map.get(key).computeIfAbsent(item.getId(), k -> new ArrayList<>());
				map.get(key).get(item.getId()).add(item.getValue());
			}
		}
	}
}