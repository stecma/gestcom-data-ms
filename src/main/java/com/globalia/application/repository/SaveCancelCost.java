package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.ItemResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveCancelCost extends AbstractSaveDao<CancelCostItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final CancelCostItem item) throws JsonProcessingException {
		return getParams(sqlType, item.getCredential(), item.getId(), getJsonHandler().toJson(item));
	}

	@Override
	protected String getSqlFile(final CancelCostItem item) {
		return SqlLoader.sqlFile.CANCEL_COST.name().toLowerCase();
	}

	@Override
	protected CancelCostItem getObject(final String json) throws IOException {
		return (CancelCostItem) getJsonHandler().fromJson(json, CancelCostItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final CancelCostItem item, final SqlLoader.sqlType sqlType) {
		response.setCancelCost(item);
	}
}