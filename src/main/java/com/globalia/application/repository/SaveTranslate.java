package com.globalia.application.repository;

import com.globalia.infraestructure.SqlLoader;
import com.globalia.enumeration.LogType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Slf4j
@Component
@RefreshScope
public class SaveTranslate {

	@Value("${logsFormat}")
	private String logsFormat;

	@Autowired
	private SqlLoader sqlLoader;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void translateSupplier(final String sqlType, final Object[] params, final String environment) {
		this.execute(sqlType, params, environment);
	}

	public void translateGroup(final String sqlType, final Object[] params, final String environment) {
		this.execute(sqlType, params, environment);
	}

	public void translateExternal(final String sqlType, final Object[] params, final String environment) {
		this.execute(sqlType, params, environment);
	}

	public Long getGroupId() {
		return this.jdbcTemplate.queryForObject("SELECT seq_gcex.nextval FROM dual", Long.class);
	}

	private void execute(final String sqlType, final Object[] params, final String environment) {
		String query = this.sqlLoader.getSql(SqlLoader.sqlFile.TRANSLATE.name(), sqlType);
		try {
			if (StringUtils.hasText(query)) {
				this.jdbcTemplate.update(query, params);
			} else {
				SaveTranslate.log.error(this.logsFormat, environment, LogType.INFO.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Error: Empty query sentence");
			}
		} catch (DataIntegrityViolationException data) {
			SaveTranslate.log.error(this.logsFormat, environment, LogType.INFO.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Object already exists");
		} catch (RuntimeException ex) {
			SaveTranslate.log.error(this.logsFormat, environment, LogType.INFO.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format("Error during credential transaction[%s] : %s", query, ex.getLocalizedMessage()));
		}
	}
}