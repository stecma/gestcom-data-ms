package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveSupplierByZone extends AbstractSaveDao<SupplierByZoneItem> {

	@Override
	protected Object[] getParams(final SqlLoader.sqlType sqlType, final SupplierByZoneItem item) throws JsonProcessingException {
		switch (sqlType) {
			case DELETE:
				return new Object[]{item.getId().split("¬")[8]};
			case UPDATE:
				return new Object[]{item.getCodpro(), this.getValue(item.getRefzge()), item.getCoduser(), this.getValue(item.getCodpai()), this.getValue(item.getCodepr()), this.getValue(item.getCodare()), this.getValue(item.getSubprv()), this.getValue(item.getCodprvext()), item.getId().split("¬")[8]};
			default:
				return new Object[]{item.getCodsis(), item.getCodpro(), this.getValue(item.getRefzge()), item.getCoduser(), item.getCoduser(), this.getValue(item.getCodpai()), this.getValue(item.getCodepr()), this.getValue(item.getCodare()), this.getValue(item.getSubprv()), this.getValue(item.getCodprvext())};
		}
	}

	@Override
	protected String getSqlFile(final SupplierByZoneItem item) {
		return SqlLoader.sqlFile.SUPPLIER_BYZONE.name().toLowerCase();
	}

	@Override
	protected SupplierByZoneItem getObject(final String json) throws IOException {
		return (SupplierByZoneItem) getJsonHandler().fromJson(json, SupplierByZoneItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final SupplierByZoneItem item, final SqlLoader.sqlType sqlType) {
		response.setSupplierByZone(item);
	}

	private String getValue(final String valueItem) {
		return StringUtils.hasText(valueItem) ? valueItem : null;
	}
}