package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.ReleaseItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveRelease extends AbstractSaveDao<ReleaseItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final ReleaseItem item) throws JsonProcessingException {
		if (sqlType == SqlLoader.sqlType.UPDATE) {
			return new Object[]{getJsonHandler().toJson(item), item.getId()};
		}
		return new Object[]{item.getId(), getJsonHandler().toJson(item)};
	}

	@Override
	protected String getSqlFile(final ReleaseItem item) {
		return SqlLoader.sqlFile.RELEASE.name().toLowerCase();
	}

	@Override
	protected ReleaseItem getObject(final String json) throws IOException {
		return (ReleaseItem) getJsonHandler().fromJson(json, ReleaseItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final ReleaseItem item, final SqlLoader.sqlType sqlType) {
		response.setRelease(item);
	}
}