package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.I18n;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@Dao
@Component
public class SaveMaster extends AbstractSaveDao<MasterItem> {

	@Value("${defaultLanguage}")
	private String defaultLanguage;

	public Object[] getParams(final SqlLoader.sqlType sqlType, final MasterItem item) throws JsonProcessingException {
		MasterType entity = MasterType.valueOf(item.getEntity());
		if (sqlType == SqlLoader.sqlType.INSERT) {
			if (entity == MasterType.SERVICE_DISTRIBUTION) {
				String service = null;
				String channel = null;
				for (MasterItem master : item.getMasters()) {
					if (MasterType.valueOf(master.getEntity()) == MasterType.SERVICE_TYPE) {
						service = master.getId();
					} else {
						channel = master.getId();
					}
					master.setCodes(null);
				}
				return new Object[]{item.getId(), this.getName(item.getNames()), service, channel, getJsonHandler().toJson(item)};
			}
			return new Object[]{item.getId(), this.getName(item.getNames()), getJsonHandler().toJson(item)};
		} else if (sqlType == SqlLoader.sqlType.UPDATE) {
			return new Object[]{this.getName(item.getNames()), getJsonHandler().toJson(item), this.getId(entity, item.getId())};
		}
		return new Object[]{getJsonHandler().toJson(item), this.getId(entity, item.getId())};
	}

	@Override
	protected String getSqlFile(final MasterItem item) {
		return String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, item.getEntity()).toLowerCase();
	}

	@Override
	protected MasterItem getObject(final String json) throws IOException {
		return (MasterItem) getJsonHandler().fromJson(json, MasterItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final MasterItem item, final SqlLoader.sqlType sqlType) {
		if (sqlType != SqlLoader.sqlType.DELETE && item.getCodes() != null) {
			this.executeChildren(this.getChildrenEntity(item, sqlType == SqlLoader.sqlType.INSERT), item);
		}
		response.setMaster(item);
	}

	private List<String> getChildrenEntity(final MasterItem item, final boolean isNew) {
		Set<String> entityChildren = new HashSet<>();
		Map<String, String> queryMap = getSqlLoader().getChildSqlMap(this.getSqlFile(item));
		for (String child : queryMap.keySet()) {
			if (child.contains(SqlLoader.sqlType.INSERT.name().toLowerCase()) || (!isNew && child.contains(SqlLoader.sqlType.DELETE.name().toLowerCase()))) {
				entityChildren.add(child.toUpperCase());
			}
		}

		List<String> sortedList = new ArrayList<>(entityChildren);
		Collections.sort(sortedList);
		return sortedList;
	}

	private String getId(final MasterType entity, final String id) {
		String code = id;
		if (entity == MasterType.BRANCH_OFFICE) {
			code = code.substring(code.lastIndexOf('¬') + 1);
		}
		return code;
	}

	private void executeChildren(final List<String> children, final MasterItem item) {
		Map<String, String> queryMap = getSqlLoader().getChildSqlMap(this.getSqlFile(item));
		if (!queryMap.isEmpty()) {
			for (String child : children) {
				this.deleteChildren(child, item.getId(), queryMap);
				this.insertChildren(child, item.getId(), item.getCodes(), queryMap);
			}
		}
	}

	private void insertChildren(final String child, final String id, final Set<PairValue> codes, final Map<String, String> queryMap) {
		// Insert N:M dependecies
		if (child.contains(SqlLoader.sqlType.INSERT.name())) {
			for (PairValue pair : codes) {
				if (child.contains(pair.getKey())) {
					for (String code : pair.getValue()) {
						getJdbcTemplate().update(queryMap.get(child.toLowerCase()), id, code);
					}
				}
			}
		}
	}

	private void deleteChildren(final String child, final String id, final Map<String, String> queryMap) {
		// Delete N:M dependecies
		if (child.contains(SqlLoader.sqlType.DELETE.name())) {
			getJdbcTemplate().update(queryMap.get(child.toLowerCase()), id);
		}
	}

	private String getName(final LinkedHashSet<I18n> names) {
		String name = "";
		for (I18n i18n : names) {
			if (this.defaultLanguage.equalsIgnoreCase(i18n.getKey())) {
				name = i18n.getValue();
			}
		}
		return name;
	}
}