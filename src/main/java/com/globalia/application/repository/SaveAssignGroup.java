package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveAssignGroup extends AbstractSaveDao<GroupCredentialItem> {

	@Override
	public Object[] getParams(final SqlLoader.sqlType sqlType, final GroupCredentialItem item) throws JsonProcessingException {
		return new Object[0];
	}

	@Override
	protected String getSqlFile(final GroupCredentialItem item) {
		return SqlLoader.sqlFile.ASSIGN_GROUP.name().toLowerCase();
	}

	@Override
	protected GroupCredentialItem getObject(final String json) throws IOException {
		return (GroupCredentialItem) getJsonHandler().fromJson(json, GroupCredentialItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final GroupCredentialItem item, final SqlLoader.sqlType sqlType) {
		response.setGroup(item);
	}

	public ItemResponse assign(final String credential, final String json) {
		ItemResponse response;
		try {
			GroupCredentialItem item = this.getObject(json);
			response = this.deleteItem(credential, item);
			if (response.getError() == null) {
				response = this.createItem(credential, item);
			}
		} catch (IOException e) {
			SaveAssignGroup.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_DES_MSG, e.getMessage()));
			response = new ItemResponse();
			response.setError(new Error(e.getLocalizedMessage(), ErrorLayer.REPOSITORY_LAYER));
		}
		return response;
	}

	public ItemResponse createItem(final String credential, final GroupCredentialItem item) {
		ItemResponse response = new ItemResponse();
		try {
			response.setError(executeDB(this.getSqlFile(item), SqlLoader.sqlType.INSERT.name(), new Object[]{credential, item.getId(), item.getSupplierId(), getJsonHandler().toJson(item)}));
			this.setResult(response, item, SqlLoader.sqlType.INSERT);
		} catch (JsonProcessingException e) {
			SaveAssignGroup.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_SER_MSG, e.getMessage()));
			response.setError(new Error());
		}
		return response;
	}

	public ItemResponse deleteItem(final String credential, final GroupCredentialItem item) {
		ItemResponse response = new ItemResponse();
		response.setError(executeDB(this.getSqlFile(item), SqlLoader.sqlType.DELETE_ASSIGN.name(), new Object[]{credential, item.getId(), item.getSupplierId()}));
		this.setResult(response, item, SqlLoader.sqlType.DELETE_ASSIGN);
		return response;
	}
}