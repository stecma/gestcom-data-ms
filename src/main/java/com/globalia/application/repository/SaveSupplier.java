package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.LogType;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Set;

@Slf4j
@Dao
@Component
public class SaveSupplier extends AbstractSaveDao<SupplierItem> {

	@Value("${defaultCompany}")
	private String defaultCompany;

	public void updateSap(final String system, final String sap, final boolean isNew) {
		if (system.contains("¬")) {
			String[] keys = system.split("¬");
			if (isNew) {
				String query = getSqlLoader().getSql(this.getSqlFile(null), SqlLoader.sqlType.INSERT_SAP_SUB.name());
				if (StringUtils.hasText(query)) {
					getJdbcTemplate().update(query, keys[0], keys[1], sap);
				}
			} else {
				String query = getSqlLoader().getSql(this.getSqlFile(null), SqlLoader.sqlType.UPDATE_SAP_SUB.name());
				if (StringUtils.hasText(query)) {
					getJdbcTemplate().update(query, sap, keys[0], keys[1]);
				}
			}
		} else {
			String query = getSqlLoader().getSql(this.getSqlFile(null), SqlLoader.sqlType.UPDATE_SAP.name());
			if (StringUtils.hasText(query)) {
				getJdbcTemplate().update(query, sap, system);
			}
		}
	}

	public void updateService(final String system, final String refser, final String tipser) {
		String query = getSqlLoader().getSql(this.getSqlFile(null), SqlLoader.sqlType.UPDATE_SERVICE.name());
		if (StringUtils.hasText(query)) {
			String sys = system;
			if (sys.contains("¬")) {
				sys = sys.split("¬")[0];
			}
			getJdbcTemplate().update(query, refser, tipser, sys);
		}
	}

	protected Object[] getParams(final SqlLoader.sqlType sqlType, final SupplierItem supplier) throws JsonProcessingException {
		if (sqlType == SqlLoader.sqlType.INSERT) {
			return new Object[]{supplier.getId(), getCode(supplier.getCodes(), MasterType.SERVICE_TYPE), getCode(supplier.getCodes(), MasterType.EXTERNAL_SYSTEM), getCode(supplier.getCodes(), MasterType.BUY_MODEL), getCode(supplier.getCodes(), MasterType.USERS), getCode(supplier.getCodes(), MasterType.CLIENT_PREF), getJsonHandler().toJson(supplier)};
		} else if (sqlType == SqlLoader.sqlType.UPDATE) {
			return new Object[]{getCode(supplier.getCodes(), MasterType.SERVICE_TYPE), getCode(supplier.getCodes(), MasterType.EXTERNAL_SYSTEM), getCode(supplier.getCodes(), MasterType.BUY_MODEL), getCode(supplier.getCodes(), MasterType.USERS), getCode(supplier.getCodes(), MasterType.CLIENT_PREF), getJsonHandler().toJson(supplier), supplier.getId()};
		} else {
			return new Object[]{getJsonHandler().toJson(supplier), supplier.getId()};
		}
	}

	@Override
	protected String getSqlFile(final SupplierItem item) {
		return SqlLoader.sqlFile.SUPPLIER.name().toLowerCase();
	}

	protected SupplierItem getObject(final String json) throws IOException {
		return (SupplierItem) getJsonHandler().fromJson(json, SupplierItem.class);
	}

	protected void setResult(final ItemResponse response, final SupplierItem item, final SqlLoader.sqlType sqlType) {
		response.setSupplier(item);
		if (sqlType != SqlLoader.sqlType.DELETE) {
			this.doSqlChildren(response, item, Set.of(MasterType.AGENCY, MasterType.TOP_DESTINATION));
		}
	}

	@Override
	protected ItemResponse execute(final SqlLoader.sqlType sqlType, final SupplierItem supplier) {
		if (supplier.getCodes() == null || supplier.getCodes().isEmpty()) {
			SaveSupplier.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Supplier without children");
			ItemResponse response = new ItemResponse();
			response.setError(new Error());
			return response;
		}

		return super.execute(sqlType, supplier);
	}

	private void doSqlChildren(final ItemResponse response, final SupplierItem supplier, final Set<MasterType> types) {
		try {
			response.setError(this.removeChildren(supplier, types));
			if (response.getError() == null) {
				response.setError(this.addChildren(supplier, types));
			}
		} catch (RuntimeException ex) {
			SaveSupplier.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_SER_MSG, ex.getMessage()));
			response.setError(new Error());
		}
	}

	private Error removeChildren(final SupplierItem item, final Set<MasterType> types) {
		for (MasterType type : types) {
			String query = getSqlLoader().getSql(this.getSqlFile(item), String.format("%s¬%s", SqlLoader.sqlType.DELETE.name().toLowerCase(), type.name().toLowerCase()));
			if (!StringUtils.hasText(query)) {
				return new Error();
			}
			getJdbcTemplate().update(query, item.getId());
		}
		return null;
	}

	private Error addChildren(final SupplierItem item, final Set<MasterType> types) {
		for (MasterType type : types) {
			Set<String> children = this.getInternalCodes(type, item.getCodes());
			if (children != null) {
				String query = getSqlLoader().getSql(this.getSqlFile(item), String.format("%s¬%s", SqlLoader.sqlType.INSERT.name().toLowerCase(), type.name().toLowerCase()));
				if (!StringUtils.hasText(query)) {
					return new Error();
				}
				this.executeChildren(children, item, type, query);
			}
		}
		return null;
	}

	private Set<String> getInternalCodes(final MasterType masterType, final Set<PairValue> values) {
		return values.stream().filter(pair -> pair.getKey().equalsIgnoreCase(masterType.name())).findFirst().<Set<String>>map(PairValue::getValue).orElse(null);
	}

	private void executeChildren(final Set<String> children, final SupplierItem item, final MasterType type, final String query) {
		for (String child : children) {
			Object[] params;
			if (type == MasterType.AGENCY) {
				params = new Object[]{item.getId(), child, this.defaultCompany};
			} else {
				params = new Object[]{item.getId(), child};
			}
			getJdbcTemplate().update(query, params);
		}
	}
}