package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.infraestructure.SqlLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveCredential extends AbstractSaveDao<CredentialItem> {

    public Object[] getParams(final SqlLoader.sqlType sqlType, final CredentialItem item) throws JsonProcessingException {
        if (sqlType == SqlLoader.sqlType.INSERT) {
            return new Object[]{item.getId(), item.getAlias(), item.getSystemId(), getCode(item.getCodes(), MasterType.BRAND), getCode(item.getCodes(), MasterType.SERVICE_TYPE), getCode(item.getCodes(), MasterType.SALES_CHANNEL), getCode(item.getCodes(), MasterType.DISTRIBUTION_MODEL), getCode(item.getCodes(), MasterType.ENVIRONMENTS), getCode(item.getCodes(), MasterType.REQUEST), getJsonHandler().toJson(item)};
        } else if (sqlType == SqlLoader.sqlType.UPDATE) {
            return new Object[]{item.getAlias(), item.getSystemId(), getCode(item.getCodes(), MasterType.BRAND), getCode(item.getCodes(), MasterType.SERVICE_TYPE), getCode(item.getCodes(), MasterType.SALES_CHANNEL), getCode(item.getCodes(), MasterType.DISTRIBUTION_MODEL), getCode(item.getCodes(), MasterType.ENVIRONMENTS), getCode(item.getCodes(), MasterType.REQUEST), getJsonHandler().toJson(item), item.getId()};
        }
        return new Object[]{getJsonHandler().toJson(item), item.getId()};
    }

    @Override
    protected String getSqlFile(final CredentialItem item) {
        return SqlLoader.sqlFile.CREDENTIAL.name().toLowerCase();
    }

    @Override
    protected CredentialItem getObject(final String json) throws IOException {
        return (CredentialItem) getJsonHandler().fromJson(json, CredentialItem.class);
    }

    @Override
    protected void setResult(final ItemResponse response, final CredentialItem item, final SqlLoader.sqlType sqlType) {
        response.setCredential(item);
    }
}