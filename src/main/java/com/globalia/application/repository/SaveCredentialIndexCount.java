package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.CredentialCounterItem;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.infraestructure.SqlLoader.sqlFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveCredentialIndexCount extends AbstractSaveDao<CredentialCounterItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final CredentialCounterItem item) throws JsonProcessingException {
		if (sqlType == SqlLoader.sqlType.INSERT) {
			return new Object[]{item.getName(), getJsonHandler().toJson(item)};
		} else if (sqlType == SqlLoader.sqlType.UPDATE) {
			return new Object[]{getJsonHandler().toJson(item),item.getName()};
		}
		return new Object[]{getJsonHandler().toJson(item), item.getName()};
	}

	@Override
	protected String getSqlFile(final CredentialCounterItem item) {
		return sqlFile.CREDENTIAL_INDEX_COUNTER.name().toLowerCase();
	}

	@Override
	protected CredentialCounterItem getObject(final String json) throws IOException {
		return (CredentialCounterItem) getJsonHandler().fromJson(json, CredentialCounterItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final CredentialCounterItem item, final SqlLoader.sqlType sqlType) {
		response.setCredentialIndexCounter(item);
	}
}