package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ItemResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveCustomer extends AbstractSaveDao<CustomerItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final CustomerItem item) throws JsonProcessingException {
		return getParams(sqlType, item.getCredential(), item.getId(), getJsonHandler().toJson(item));
	}

	@Override
	protected String getSqlFile(final CustomerItem item) {
		return SqlLoader.sqlFile.CUSTOMER.name().toLowerCase();
	}

	@Override
	protected CustomerItem getObject(final String json) throws IOException {
		return (CustomerItem) getJsonHandler().fromJson(json, CustomerItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final CustomerItem item, final SqlLoader.sqlType sqlType) {
		response.setCustomer(item);
	}
}