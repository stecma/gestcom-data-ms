package com.globalia.infraestructure;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@RefreshScope
@Service
public class SqlLoader {

	public static final String KEY_FORMAT = "%s¬%s";
	private final ExecutorService pool = Executors.newFixedThreadPool(sqlFile.values().length);
	private final Map<String, Map<String, String>> queriesMap = new HashMap<>();
	private final Map<String, Map<String, String>> childQueriesMap = new HashMap<>();
	private final Map<String, String> queryMap = new HashMap<>();

	@Value("${cancelcostSql}")
	private String cancelcostSql;
	@Value("${externalCredentialSql}")
	private String externalCredentialSql;
	@Value("${credentialSql}")
	private String credentialSql;
	@Value("${customerSql}")
	private String customerSql;
	@Value("${groupCredentialSql}")
	private String groupCredentialSql;
	@Value("${credentialGroupSql}")
	private String credentialGroupSql;
	@Value("${releaseSql}")
	private String releaseSql;
	@Value("${supplierSQL}")
	private String supplierSQL;
	@Value("${tradePolicySql}")
	private String tradePolicySql;
	@Value("${supplierByZoneSql}")
	private String supplierByZoneSql;
	@Value("${masterSql}")
	private String masterSql;
	@Value("${translateSql}")
	private String translateSql;
	@Value("${currencyByCountrySql}")
	private String currencyByCountrySql;
	@Value("${currencySql}")
	private String currencySql;
	@Value("${credentialAliasCounter}")
	private String credentialAliasCounter;
	@Value("${credentialIndexCounter}")
	private String credentialIndexCounter;

	public void launch() throws InterruptedException {
		this.loader();
		TimeUnit.SECONDS.sleep(3);
		this.loaderMaster();
	}

	void loader() {
		CompletableFuture.allOf(Arrays.stream(sqlFile.values()).map(v -> this.loadSQLQueries(this.getValue(v), v)).toArray(CompletableFuture[]::new));
	}

	void loaderMaster() {
		CompletableFuture.supplyAsync(() -> {
			String queries = this.getValue(sqlFile.MASTER);
			if (StringUtils.hasText(queries)) {
				JsonObject json = (JsonObject) JsonParser.parseString(queries);
				for (JsonElement entity : json.get("entities").getAsJsonArray()) {
					String file = String.format(SqlLoader.KEY_FORMAT, sqlFile.MASTER, entity.getAsJsonObject().get("entity").getAsString()).toLowerCase();
					SqlLoader.log.info(String.format("Init load master queries from %s", file));
					this.getSqlSentence(file, String.format("{\"queries\": %s}", entity.getAsJsonObject().get("queries").toString()), entity.getAsJsonObject().get("children"));
					SqlLoader.log.info(String.format("Load master complete %s", file));
				}
			}
			return null;
		});
	}

	public String getSql(final String file, final String sqlType) {
		Map<String, String> map = this.queriesMap.computeIfPresent(file.toLowerCase(), (k, v) -> v);
		if (map != null) {
			return map.computeIfPresent(sqlType.toLowerCase(), (k, v) -> v);
		}
		return null;
	}

	public Map<String, String> getChildSqlMap(final String file) {
		return this.childQueriesMap.containsKey(file) ? this.childQueriesMap.get(file) : new HashMap<>();
	}

	@Async
	CompletableFuture<Void> loadSQLQueries(final String queries, final sqlFile file) {
		return CompletableFuture.supplyAsync(() -> {
			if (file != sqlFile.MASTER && StringUtils.hasText(queries)) {
				SqlLoader.log.info(String.format("Init load queries from %s", file.name()));
				if (!queries.equalsIgnoreCase(this.queryMap.get(file.name().toLowerCase()))) {
					this.getSqlSentence(file.name().toLowerCase(), queries, null);
				}
				SqlLoader.log.info(String.format("Load complete %s", file.name()));
			}
			return null;
		}, this.pool);
	}

	private void getSqlSentence(final String file, final String queries, final JsonElement children) {
		if (!this.queryMap.containsKey(file)) {
			this.queryMap.put(file, "");
			this.queriesMap.put(file, null);
		}
		this.queryMap.replace(file, queries);

		JsonObject json = (JsonObject) JsonParser.parseString(queries);
		Map<String, String> map = new HashMap<>();
		for (JsonElement query : json.get("queries").getAsJsonArray()) {
			map.computeIfAbsent(query.getAsJsonObject().get("type").getAsString().toLowerCase(), k -> query.getAsJsonObject().get("sql").getAsString());
		}
		this.queriesMap.replace(file, map);
		this.getSQLChildQueries(file, children);
	}

	private void getSQLChildQueries(final String file, final JsonElement children) {
		if (children != null) {
			if (!this.childQueriesMap.containsKey(file)) {
				this.childQueriesMap.put(file, null);
			}

			Map<String, String> map = null;
			for (JsonElement child : children.getAsJsonArray()) {
				if (map == null) {
					map = new HashMap<>();
				}
				map.put(child.getAsJsonObject().get("type").getAsString(), child.getAsJsonObject().get("sql").getAsString());
			}
			this.childQueriesMap.replace(file, map);
		}
	}

	private String getValue(final sqlFile file) {
		switch (file) {
			case CANCEL_COST:
				return this.cancelcostSql;
			case CONNECTOR:
				return this.externalCredentialSql;
			case CREDENTIAL:
				return this.credentialSql;
			case CUSTOMER:
				return this.customerSql;
			case GROUP:
				return this.groupCredentialSql;
			case ASSIGN_GROUP:
				return this.credentialGroupSql;
			case RELEASE:
				return this.releaseSql;
			case SUPPLIER:
				return this.supplierSQL;
			case TRADE:
				return this.tradePolicySql;
			case SUPPLIER_BYZONE:
				return this.supplierByZoneSql;
			case TRANSLATE:
				return this.translateSql;
			case CURRENCY_BYCOUNTRY:
				return this.currencyByCountrySql;
			case CURRENCY:
				return this.currencySql;
			case CREDENTIAL_INDEX_COUNTER:
				return this.credentialIndexCounter;
			case CREDENTIAL_ALIAS_COUNTER:
				return this.credentialAliasCounter;
			default:
				return this.masterSql;
		}
	}

	public enum sqlFile {
		CANCEL_COST(false),
		CONNECTOR(true),
		CREDENTIAL(true),
		CUSTOMER(false),
		GROUP(true),
		ASSIGN_GROUP(false),
		RELEASE(true),
		SUPPLIER(true),
		SUPPLIER_BYZONE(true),
		TRADE(false),
		MASTER(true),
		TRANSLATE(false),
		CURRENCY_BYCOUNTRY(true),
		CURRENCY(false),
		CREDENTIAL_INDEX_COUNTER(true),
		CREDENTIAL_ALIAS_COUNTER(true);

		public final boolean simpleMap;

		sqlFile(final boolean simpleMap) {
			this.simpleMap = simpleMap;
		}
	}

	public enum sqlType {
		QUERY,
		QUERY_ALL,
		QUERY_ALL_SCAN,
		INSERT,
		UPDATE,
		DELETE,
		DELETE_ASSIGN,
		INSERT_SAP_SUB,
		UPDATE_SAP_SUB,
		UPDATE_SAP,
		UPDATE_SERVICE
	}
}