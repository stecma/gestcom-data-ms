package com.globalia.infraestructure;

import com.globalia.Context;
import com.globalia.application.repository.GetItems;
import com.globalia.dto.ItemDao;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.enumeration.LogType;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@RefreshScope
@Service
public class RedisLoader {

	public static final String REDIS_KEY_FORMAT = "%s:%s";
	private static final String ERROR_MSG = "Error: %s";
	private final ExecutorService pool = Executors.newFixedThreadPool(SqlLoader.sqlFile.values().length);
	private final ExecutorService poolMaster = Executors.newFixedThreadPool(MasterType.values().length);

	@Value("${logsFormat}")
	private String logsFormat;
	@Value("${masterPermission}")
	private String masterPermission;
	@Value("${supplierMapKey}")
	private String supplierMapKey;
	@Value("${redis.cancelcostKey}")
	private String cancelcostKey;
	@Value("${redis.connectorsKey}")
	private String connectorsKey;
	@Value("${redis.credentialsKey}")
	private String credentialsKey;
	@Value("${redis.customersKey}")
	private String customersKey;
	@Value("${redis.groupsKey}")
	private String groupsKey;
	@Value("${redis.credGroupsKey}")
	private String credGroupsKey;
	@Value("${redis.releasesKey}")
	private String releasesKey;
	@Value("${redis.suppliersKey}")
	private String suppliersKey;
	@Value("${redis.tradesKey}")
	private String tradesKey;
	@Value("${redis.byzonesKey}")
	private String byzonesKey;
	@Value("${redis.currencyByCountryKey}")
	private String currencyByCountryKey;
	@Value("${redis.currencyKey}")
	private String currencyKey;
	@Value("${redis.mastersKey}")
	private String mastersKey;
	@Value("${redis.credentialIndexCounterKey}")
	private String credentialIndexCounterKey;
	@Value("${redis.credentialAliasCounterKey}")
	private String credentialAliasCounterKey;
	private String scanKey;
	private String permissionKey;
	private String mapKeyRedisKey;

	@Autowired
	private GetItems getItems;
	@Autowired
	private RedisClient client;
	@Autowired
	private Context context;

	public void launch() throws InterruptedException {
		this.client.flushDb();
		this.loaderMapKey();
		this.loaderPermission();
		TimeUnit.SECONDS.sleep(3);
		this.loader();
		TimeUnit.SECONDS.sleep(3);
		this.loaderMaster();
	}

	public void reload() throws InterruptedException {
		this.client.flushDb();
		this.launch();
	}
	
	public void updateSupplierByZone(final String system, final String sap) {
		String sys = system;
		String subSys = null;
		if (system.contains("¬")) {
			String[] keys = system.split("¬");
			sys = keys[0];
			subSys = keys[1];
		}

		SupplierByZoneItem filterZone = new SupplierByZoneItem();
		filterZone.setId("new");
		filterZone.setCodsis(sys);
		filterZone.setCodpro(sap);
		filterZone.setSubprv(subSys);

		this.addRedis(this.getItems.getSupplierByZone(filterZone), this.getRediskey(this.byzonesKey, false));
	}

	public void updateSystem(final String system) {
		MasterItem filter = new MasterItem();
		filter.setId(system);
		filter.setEntity(MasterType.EXTERNAL_SYSTEM.name());
		this.addRedis(this.getItems.getMaster(filter), String.format(RedisAccess.REDIS_KEY_FORMAT, this.getRediskey(this.mastersKey, false), MasterType.EXTERNAL_SYSTEM.name()));
	}

	public String getRediskey(final String redisKey, final boolean isNew) {
		String pattern = RedisLoader.REDIS_KEY_FORMAT;
		if (isNew) {
			pattern = "%s:%s_new";
		}
		return String.format(pattern, this.context.getEnvironment(), redisKey);
	}

	void loader() {
		CompletableFuture.allOf(Arrays.stream(SqlLoader.sqlFile.values()).map(this::loadRedis).toArray(CompletableFuture[]::new));
	}

	void loaderMaster() {
		this.scanKey = String.format("%s:scan", this.mastersKey);
		CompletableFuture.allOf(Arrays.stream(MasterType.values()).map(this::loaderMaster).toArray(CompletableFuture[]::new));
	}

	void loaderMapKey() {
		this.mapKeyRedisKey = String.format("%s:mapKey", this.suppliersKey);
		this.loadMapKey();
	}

	void loaderPermission() {
		this.permissionKey = String.format("%s:permission", this.mastersKey);
		this.loadPermission();
	}

	@Async
	CompletableFuture<Void> loadRedis(final SqlLoader.sqlFile file) {
		return CompletableFuture.supplyAsync(() -> {
			if (file != SqlLoader.sqlFile.MASTER) {
				String redisKey = this.getKey(file);
				this.addAllRedis(redisKey, this.getItems.getItems(file), file.simpleMap);
			}
			return null;
		}, this.pool);
	}

	@Async
	CompletableFuture<Void> loaderMaster(final MasterType entity) {
		return CompletableFuture.supplyAsync(() -> {
			MasterItem filter = new MasterItem();
			filter.setEntity(entity.name());

			String file = String.format(SqlLoader.KEY_FORMAT, SqlLoader.sqlFile.MASTER, entity.name()).toLowerCase();
			this.addAllRedis(String.format(RedisLoader.REDIS_KEY_FORMAT, this.mastersKey, entity.name()), this.getItems.getItems(file, false), true);
			this.addAllDepRedis(file, entity.name());
			this.addAllDesc(file, entity.name());
			return null;
		}, this.poolMaster);
	}

	private void addAllDepRedis(final String file, final String entity) {
		Map<String, Map<String, List<Object>>> map = this.getItems.getAllChildItems(file);
		if (!map.isEmpty()) {
			String redisKey = this.getRediskey(String.format(RedisLoader.REDIS_KEY_FORMAT, this.mastersKey, entity), false);
			for (Map.Entry<String, Map<String, List<Object>>> entry : map.entrySet()) {
				Map<String, List<Object>> valueMap = entry.getValue();
				for (Map.Entry<String, List<Object>> entryValue : valueMap.entrySet()) {
					try {
						this.client.addList(String.format("%s:%s:%s", redisKey, entry.getKey(), entryValue.getKey()), entryValue.getValue());
					} catch (RuntimeException r) {
						RedisLoader.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), r.getMessage());
					}
				}
			}
		}
	}

	private void addAllDesc(final String file, final String entity) {
		List<ItemDao> allItems = this.getItems.getItems(file, true);
		if (!allItems.isEmpty()) {
			List<Object> list = new ArrayList<>();
			for (ItemDao item : allItems) {
				list.add(item.getValue());
			}

			try {
				String temporaryKey = this.getRediskey(String.format(RedisLoader.REDIS_KEY_FORMAT, this.scanKey, entity), true);
				this.client.addList(temporaryKey, list);
				this.client.rename(temporaryKey, this.getRediskey(String.format(RedisLoader.REDIS_KEY_FORMAT, this.scanKey, entity), false));
			} catch (RuntimeException r) {
				RedisLoader.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), r.getMessage());
			}
		}
	}

	private void loadPermission() {
		if (StringUtils.hasText(this.masterPermission)) {
			Map<String, Object> mapValues = new HashMap<>();
			JsonObject json = (JsonObject) JsonParser.parseString(this.masterPermission);
			for (JsonElement entity : json.get("entities").getAsJsonArray()) {
				mapValues.put(entity.getAsJsonObject().get("entity").getAsString().toUpperCase(), entity.getAsJsonObject().get("permission").getAsString());
			}
			this.loadRedis(this.permissionKey, mapValues);
		}
	}

	private void loadMapKey() {
		if (StringUtils.hasText(this.supplierMapKey)) {
			JsonObject json = (JsonObject) JsonParser.parseString(this.supplierMapKey);
			Map<String, Object> mapValues = new HashMap<>();
			for (JsonElement entities : json.get("entities").getAsJsonArray()) {
				String entity = entities.getAsJsonObject().get("entity").getAsString();
				JsonElement contents = entities.getAsJsonObject().get("content");
				for (JsonElement content : contents.getAsJsonArray()) {
					String key = String.format(RedisLoader.REDIS_KEY_FORMAT, entity, content.getAsJsonObject().get("field").getAsString());
					mapValues.put(key, content.toString());
				}
			}
			this.loadRedis(this.mapKeyRedisKey, mapValues);
		}
	}

	private void loadRedis(final String redisKey, final Map<String, Object> values) {
		if (!values.isEmpty()) {
			try {
				String temporaryKey = this.getRediskey(redisKey, true);
				this.client.addMap(temporaryKey, values);
				this.client.rename(temporaryKey, this.getRediskey(redisKey, false));
			} catch (RuntimeException r) {
				RedisLoader.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(RedisLoader.ERROR_MSG, r.getLocalizedMessage()));
			}
		}
	}

	private String getKey(final SqlLoader.sqlFile file) {
		switch (file) {
			case CANCEL_COST:
				return this.cancelcostKey;
			case CONNECTOR:
				return this.connectorsKey;
			case CREDENTIAL:
				return this.credentialsKey;
			case CUSTOMER:
				return this.customersKey;
			case GROUP:
				return this.groupsKey;
			case ASSIGN_GROUP:
				return this.credGroupsKey;
			case RELEASE:
				return this.releasesKey;
			case SUPPLIER:
				return this.suppliersKey;
			case TRADE:
				return this.tradesKey;
			case CURRENCY:
				return this.currencyKey;
			case CURRENCY_BYCOUNTRY:
				return this.currencyByCountryKey;
			case CREDENTIAL_ALIAS_COUNTER:
				return this.credentialAliasCounterKey;
			case CREDENTIAL_INDEX_COUNTER:
				return this.credentialIndexCounterKey;
			default:
				return this.byzonesKey;
		}
	}

	private void addAllRedis(final String baseKey, final List<ItemDao> items, final boolean simpleMap) {
		Map<String, Map<String, Object>> map = this.getMap(items, simpleMap);
		if (!map.isEmpty()) {
			for (Map.Entry<String, Map<String, Object>> entry : map.entrySet()) {
				String finalKey = baseKey;
				if (StringUtils.hasText(entry.getKey())) {
					finalKey = String.format(RedisLoader.REDIS_KEY_FORMAT, baseKey, entry.getKey());
				}
				this.addMap(finalKey, entry.getValue());
			}
		}
	}

	private Map<String, Map<String, Object>> getMap(final List<ItemDao> items, final boolean simpleMap) {
		Map<String, Map<String, Object>> map = new HashMap<>();
		if (!items.isEmpty()) {
			String keyMap = "";
			for (ItemDao item : items) {
				String key = item.getId();
				if (!simpleMap) {
					String[] keys = item.getId().split(":");
					keyMap = keys[0];
					key = keys[1];
				}

				map.computeIfAbsent(keyMap, k -> new HashMap<>());
				map.get(keyMap).computeIfAbsent(key, k -> item.getValue());
			}
		}
		return map;
	}

	private void addMap(final String finalKey, final Map<String, Object> map) {
		try {
			String temporaryKey = this.getRediskey(finalKey, true);
			String redisKey = this.getRediskey(finalKey, false);
			RedisLoader.log.info(String.format("Load redis %s", temporaryKey));
			this.client.addMap(temporaryKey, map);
			this.client.rename(temporaryKey, redisKey);
			RedisLoader.log.info(String.format("Completed %s", redisKey));
		} catch (RuntimeException r) {
			RedisLoader.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(RedisLoader.ERROR_MSG, r.getLocalizedMessage()));
		}
	}

	private void addRedis(final ItemDao item, final String rediskey) {
		if (item != null) {
			try {
				this.client.add(rediskey, item.getId(), item.getValue());
			} catch (RuntimeException r) {
				RedisLoader.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(RedisLoader.ERROR_MSG, r.getLocalizedMessage()));
			}
		}
	}
}