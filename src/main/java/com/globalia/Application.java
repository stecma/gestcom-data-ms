package com.globalia;

import com.globalia.infraestructure.RedisLoader;
import com.globalia.infraestructure.SqlLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@EnableConfigurationProperties
@EnableAutoConfiguration
@SpringBootApplication
@Slf4j
public class Application implements ApplicationRunner {

	@Autowired
	private SqlLoader sqlLoader;
	@Autowired
	private RedisLoader redisLoader;

	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}

	public void run(final ApplicationArguments args) throws InterruptedException {
		this.sqlLoader.launch();
		TimeUnit.SECONDS.sleep(3);
		this.redisLoader.launch();
		Application.log.info("Application started with command-line arguments: {}", Arrays.toString(args.getSourceArgs()));
	}
}